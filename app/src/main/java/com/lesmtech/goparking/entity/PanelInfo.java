package com.lesmtech.goparking.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Rindt
 * @version 0.1
 * @since 10/4/15
 */
public class PanelInfo {

    @SerializedName("BerthCount")
    private int berthcount;

    @SerializedName("State0Count")
    private int numberOfLeft;

    @SerializedName("State1Count")
    private int numberOfOccupied;

    @SerializedName("State2Count")
    private int numberOfReserved;

    @SerializedName("State3Count")
    private int numberOfLeaving;

    public int getNumberOfLeaving() {
        return numberOfLeaving;
    }

    public void setNumberOfLeaving(int numberOfLeaving) {
        this.numberOfLeaving = numberOfLeaving;
    }

    public int getBerthcount() {
        return berthcount;
    }

    public void setBerthcount(int berthcount) {
        this.berthcount = berthcount;
    }

    public int getNumberOfLeft() {
        return numberOfLeft;
    }

    public void setNumberOfLeft(int numberOfLeft) {
        this.numberOfLeft = numberOfLeft;
    }

    public int getNumberOfOccupied() {
        return numberOfOccupied;
    }

    public void setNumberOfOccupied(int numberOfOccupied) {
        this.numberOfOccupied = numberOfOccupied;
    }

    public int getNumberOfReserved() {
        return numberOfReserved;
    }

    public void setNumberOfReserved(int numberOfReserved) {
        this.numberOfReserved = numberOfReserved;
    }

    public PanelInfo(int berthcount, int numberOfLeft, int numberOfOccupied, int numberOfReserved, int numberOfLeaving) {
        this.berthcount = berthcount;
        this.numberOfLeft = numberOfLeft;
        this.numberOfOccupied = numberOfOccupied;
        this.numberOfReserved = numberOfReserved;
        this.numberOfLeaving = numberOfLeaving;
    }
}
