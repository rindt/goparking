package com.lesmtech.goparking.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Rindt
 * @version 0.1
 * @since 12/14/15
 */
public class RepayObject {

    @SerializedName("Id")
    String id;

    @SerializedName("OrderNumber")
    String ordernumber;

    @SerializedName("RecordId")
    String recordId;

    @SerializedName("ParkId")
    int parkId;

    @SerializedName("ServicePersonnelId")
    int servicePersonnelId;

    @SerializedName("MemberId")
    int memberId;

    @SerializedName("CouponId")
    int couponId;

    @SerializedName("PlateNumber")
    String plateNumber;

    @SerializedName("TotalSum")
    double totalSum;

    @SerializedName("Coupon")
    double coupon;
    @SerializedName("Consumption")
    double consumption;

    @SerializedName("CreateTime")
    String createTime;

    @SerializedName("ProcessTime")
    String processTime;

    @SerializedName("FailureTime")
    String failureTime;

    @SerializedName("Type")
    int type;

    @SerializedName("PaymentState")
    int paymentState;

    @SerializedName("State")
    int state;

    @SerializedName("RefundState")
    int refundState;

    public RepayObject(String id, String ordernumber, String recordId, int parkId, int servicePersonnelId, int memberId, int couponId, String plateNumber, double totalSum, double coupon, double consumption, String createTime, String processTime, String failureTime, int type, int paymentState, int state, int refundState) {
        this.id = id;
        this.ordernumber = ordernumber;
        this.recordId = recordId;
        this.parkId = parkId;
        this.servicePersonnelId = servicePersonnelId;
        this.memberId = memberId;
        this.couponId = couponId;
        this.plateNumber = plateNumber;
        this.totalSum = totalSum;
        this.coupon = coupon;
        this.consumption = consumption;
        this.createTime = createTime;
        this.processTime = processTime;
        this.failureTime = failureTime;
        this.type = type;
        this.paymentState = paymentState;
        this.state = state;
        this.refundState = refundState;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public int getParkId() {
        return parkId;
    }

    public void setParkId(int parkId) {
        this.parkId = parkId;
    }

    public int getServicePersonnelId() {
        return servicePersonnelId;
    }

    public void setServicePersonnelId(int servicePersonnelId) {
        this.servicePersonnelId = servicePersonnelId;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public int getCouponId() {
        return couponId;
    }

    public void setCouponId(int couponId) {
        this.couponId = couponId;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(double totalSum) {
        this.totalSum = totalSum;
    }

    public double getCoupon() {
        return coupon;
    }

    public void setCoupon(double coupon) {
        this.coupon = coupon;
    }

    public double getConsumption() {
        return consumption;
    }

    public void setConsumption(double consumption) {
        this.consumption = consumption;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getProcessTime() {
        return processTime;
    }

    public void setProcessTime(String processTime) {
        this.processTime = processTime;
    }

    public String getFailureTime() {
        return failureTime;
    }

    public void setFailureTime(String failureTime) {
        this.failureTime = failureTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getPaymentState() {
        return paymentState;
    }

    public void setPaymentState(int paymentState) {
        this.paymentState = paymentState;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getRefundState() {
        return refundState;
    }

    public void setRefundState(int refundState) {
        this.refundState = refundState;
    }
}
