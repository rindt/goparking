package com.lesmtech.goparking.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Rindt
 * @version 0.1
 * @since 10/5/15
 */
public class Berth {

    @SerializedName("Id")
    int id;

    @SerializedName("ParkId")
    int parkId;

    @SerializedName("BerthId")
    int berthId;

    @SerializedName("BerthNum")
    int berthNum;

    @SerializedName("PlateNumber")
    String plateNumber;

    @SerializedName("StartTime")
    String startTime;

    @SerializedName("EndTime")
    String endTime;

    /**
     * 0, 空
     * 1, 用
     * 2，预定
     */
    @SerializedName("State")
    int state;

    @SerializedName("Explain")
    String explain;

    @SerializedName("BerthType")
    int berthType;


    /**
     * For certain api..
     */
    @SerializedName("OrderInfoId")
    int orderInfoId;

    @SerializedName("Type")
    int type;

    @SerializedName("Consumption")
    int consumption;

    @SerializedName("ProcessTime")
    String processTime;

    public Berth(int id, int parkId, int berthId, int berthNum, String plateNumber, String startTime, String endTime, int state, String explain, int berthType, int orderInfoId, int type, int consumption, String processTime) {
        this.id = id;
        this.parkId = parkId;
        this.berthId = berthId;
        this.berthNum = berthNum;
        this.plateNumber = plateNumber;
        this.startTime = startTime;
        this.endTime = endTime;
        this.state = state;
        this.explain = explain;
        this.berthType = berthType;
        this.orderInfoId = orderInfoId;
        this.type = type;
        this.consumption = consumption;
        this.processTime = processTime;
    }

    public Berth(){

    }

    public int getConsumption() {
        return consumption;
    }

    public void setConsumption(int consumption) {
        this.consumption = consumption;
    }

    public String getProcessTime() {
        return processTime;
    }

    public void setProcessTime(String processTime) {
        this.processTime = processTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getOrderInfoId() {
        return orderInfoId;
    }

    public void setOrderInfoId(int orderInfoId) {
        this.orderInfoId = orderInfoId;
    }

    public int getBerthNum() {
        return berthNum;
    }

    public void setBerthNum(int berthNum) {
        this.berthNum = berthNum;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public int getBerthType() {
        return berthType;
    }

    public void setBerthType(int berthType) {
        this.berthType = berthType;
    }

    public int getBerthId() {
        return berthId;
    }

    public void setBerthId(int berthId) {
        this.berthId = berthId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParkId() {
        return parkId;
    }

    public void setParkId(int parkId) {
        this.parkId = parkId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
