package com.lesmtech.goparking.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Rindt
 * @version 0.1
 * @since 9/27/15
 */
public class Employee {

    public static String ID = "id";

    public static String JOBNUM = "jobNum";

    public static String PASSWORD = "password";

    public static String FULLNAME = "fullName";

    public static String PHOTOURL = "photoUrl";

    public static String IDCARDNUM = "idCardNum";

    public static String TELPHONE = "telPhone";

    public static String WORKINGPROPERTY = "workingProperty";

    public static String PARKID = "parkId";

    public static String LANDINGTIME = "landingtime";

    // Error Params
    @SerializedName("RowsCount")
    private String rowsCount;

    @SerializedName("Msg")
    private String msg;

    @SerializedName("Data")
    private String data;

    @SerializedName("Id")
    private long id;
    @SerializedName("JobNum")
    private String jobNum;
    @SerializedName("PassWord")
    private String password;
    @SerializedName("FullName")
    private String fullName;
    @SerializedName("PhotoUrl")
    private String photoUrl;
    @SerializedName("IdCardNum")
    private String idCardNum;
    @SerializedName("TelPhone")
    private String telPhone;
    @SerializedName("WorkingProperty")
    private int workingProperty;
    @SerializedName("ParkId")
    private int parkId;
    @SerializedName("LoginTime")
    private String landingTime;

    public String getLandingTime() {
        return landingTime;
    }

    public void setLandingTime(String landingTime) {
        this.landingTime = landingTime;
    }

    public String getRowsCount() {
        return rowsCount;
    }

    public void setRowsCount(String rowsCount) {
        this.rowsCount = rowsCount;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getJobNum() {
        return jobNum;
    }

    public void setJobNum(String jobNum) {
        this.jobNum = jobNum;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getIdCardNum() {
        return idCardNum;
    }

    public void setIdCardNum(String idCardNum) {
        this.idCardNum = idCardNum;
    }

    public String getTelPhone() {
        return telPhone;
    }

    public void setTelPhone(String telPhone) {
        this.telPhone = telPhone;
    }

    public int getWorkingProperty() {
        return workingProperty;
    }

    public void setWorkingProperty(int workingProperty) {
        this.workingProperty = workingProperty;
    }

    public int getParkId() {
        return parkId;
    }

    public void setParkId(int parkId) {
        this.parkId = parkId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
