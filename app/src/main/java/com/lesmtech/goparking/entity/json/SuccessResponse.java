package com.lesmtech.goparking.entity.json;

import com.google.gson.annotations.SerializedName;

/**
 * @author Rindt
 * @version 0.1
 * @since 9/30/15
 */
public class SuccessResponse {

    @SerializedName("RowsCount")
    private int rowsCount;

    @SerializedName("Msg")
    private String msg;

    @SerializedName("Data")
    private String data;

    public SuccessResponse(int rowsCount, String msg, String data) {
        this.rowsCount = rowsCount;
        this.msg = msg;
        this.data = data;
    }

    public int getRowsCount() {
        return rowsCount;
    }

    public void setRowsCount(int rowsCount) {
        this.rowsCount = rowsCount;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
