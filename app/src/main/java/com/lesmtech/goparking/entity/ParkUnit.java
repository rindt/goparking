package com.lesmtech.goparking.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Rindt
 * @version 0.1
 * @since 10/3/15
 */
public class ParkUnit {


    @SerializedName("Id")
    private int id;

    @SerializedName("ParkId")
    private int parkName;

    @SerializedName("PlateNumber")
    private String plateNumber;

    @SerializedName("StartTime")
    private String startTime;

    @SerializedName("EndTime")
    private String endTime;

    @SerializedName("State")
    private int state;

    public ParkUnit(int id, int parkName, String plateNumber, String startTime, String endTime, int state) {
        this.id = id;
        this.parkName = parkName;
        this.plateNumber = plateNumber;
        this.startTime = startTime;
        this.endTime = endTime;
        this.state = state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParkName() {
        return parkName;
    }

    public void setParkName(int parkName) {
        this.parkName = parkName;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }


}

