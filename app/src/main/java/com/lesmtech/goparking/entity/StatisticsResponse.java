package com.lesmtech.goparking.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Rindt
 * @version 0.1
 * @since 10/19/15
 */
public class StatisticsResponse {

    @SerializedName("ConsumptionCount")
    long revenue;
    @SerializedName("TypeCount")
    int reserveAmount;
    @SerializedName("CarCountT")
    int carAmount;

    public StatisticsResponse(long revenue, int reserveAmount, int carAmount) {
        this.revenue = revenue;
        this.reserveAmount = reserveAmount;
        this.carAmount = carAmount;
    }

    public long getRevenue() {
        return revenue;
    }

    public void setRevenue(long revenue) {
        this.revenue = revenue;
    }

    public int getReserveAmount() {
        return reserveAmount;
    }

    public void setReserveAmount(int reserveAmount) {
        this.reserveAmount = reserveAmount;
    }

    public int getCarAmount() {
        return carAmount;
    }

    public void setCarAmount(int carAmount) {
        this.carAmount = carAmount;
    }
}
