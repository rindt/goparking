package com.lesmtech.goparking.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Rindt
 * @version 0.1
 * @since 11/20/15
 */
public class CashRecord {

    int Id;

    int EmployeeId;

    @SerializedName("ParkId")
    int parkId;

    @SerializedName("Amount")
    double amount;

    @SerializedName("OrderInfoId")
    int orderInfoId;

    @SerializedName("CreateTime")
    String createTime;

    @SerializedName("PlateNumber")
    String plateNumber;

    public CashRecord(int id, int employeeId, int parkId, double amount, int orderInfoId, String createTime, String plateNumber) {
        Id = id;
        EmployeeId = employeeId;
        this.parkId = parkId;
        this.amount = amount;
        this.orderInfoId = orderInfoId;
        this.createTime = createTime;
        this.plateNumber = plateNumber;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(int employeeId) {
        EmployeeId = employeeId;
    }

    public int getParkId() {
        return parkId;
    }

    public void setParkId(int parkId) {
        this.parkId = parkId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getOrderInfoId() {
        return orderInfoId;
    }

    public void setOrderInfoId(int orderInfoId) {
        this.orderInfoId = orderInfoId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }
}
