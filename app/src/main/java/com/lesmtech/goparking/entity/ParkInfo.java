package com.lesmtech.goparking.entity;

import com.google.gson.annotations.SerializedName;

/**
 * The object to store Park Infomation
 * @author Rindt
 * @version 0.1
 * @since 9/27/15
 */
public class ParkInfo {

    @SerializedName("Id")
    private long id;

    @SerializedName("ParkName")
    private String parkName;

    @SerializedName("Address")
    private String address;

    @SerializedName("PLatitude")
    private String pLatitude;

    @SerializedName("PLongitude")
    private String pLongtitude;

    @SerializedName("Position")
    private String position;

    @SerializedName("Belonging")
    private String belonging;

    @SerializedName("FristPrice")
    private long fristPrice;

    @SerializedName("PerHourPrice")
    private long perHourPrice;

    @SerializedName("NPerHourPrice")
    private long nPerHourPrice;

    @SerializedName("NCapPrice")
    private long nCapPrice;

    @SerializedName("Type")
    private int type;

    @SerializedName("Nature")
    private int nature;

    @SerializedName("Capacity")
    private int capacity;

    @SerializedName("CADUrl")
    private String cadUrl;

    public ParkInfo() {

    }

    public ParkInfo(long id, String parkName, String address, String pLatitude, String pLongtitude, String position, String belonging, long fristPrice, long perHourPrice, long nPerHourPrice, long nCapPrice, int type, int nature, int capacity, String cadUrl) {
        this.id = id;
        this.parkName = parkName;
        this.address = address;
        this.pLatitude = pLatitude;
        this.pLongtitude = pLongtitude;
        this.position = position;
        this.belonging = belonging;
        this.fristPrice = fristPrice;
        this.perHourPrice = perHourPrice;
        this.nPerHourPrice = nPerHourPrice;
        this.nCapPrice = nCapPrice;
        this.type = type;
        this.nature = nature;
        this.capacity = capacity;
        this.cadUrl = cadUrl;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getParkName() {
        return parkName;
    }

    public void setParkName(String parkName) {
        this.parkName = parkName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getpLatitude() {
        return pLatitude;
    }

    public void setpLatitude(String pLatitude) {
        this.pLatitude = pLatitude;
    }

    public String getpLongtitude() {
        return pLongtitude;
    }

    public void setpLongtitude(String pLongtitude) {
        this.pLongtitude = pLongtitude;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getBelonging() {
        return belonging;
    }

    public void setBelonging(String belonging) {
        this.belonging = belonging;
    }

    public long getFristPrice() {
        return fristPrice;
    }

    public void setFristPrice(long fristPrice) {
        this.fristPrice = fristPrice;
    }

    public long getPerHourPrice() {
        return perHourPrice;
    }

    public void setPerHourPrice(long perHourPrice) {
        this.perHourPrice = perHourPrice;
    }

    public long getnPerHourPrice() {
        return nPerHourPrice;
    }

    public void setnPerHourPrice(long nPerHourPrice) {
        this.nPerHourPrice = nPerHourPrice;
    }

    public long getnCapPrice() {
        return nCapPrice;
    }

    public void setnCapPrice(long nCapPrice) {
        this.nCapPrice = nCapPrice;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getNature() {
        return nature;
    }

    public void setNature(int nature) {
        this.nature = nature;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getCadUrl() {
        return cadUrl;
    }

    public void setCadUrl(String cadUrl) {
        this.cadUrl = cadUrl;
    }
}
