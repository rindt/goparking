package com.lesmtech.goparking.entity;

/**
 * @author Rindt
 * @version 0.1
 * @since 11/22/15
 */
public class Order {

    long Id;
    int ParkId;
    int EmployeeId;
    int MemberId;
    String PlateNumber;
    int Type;
    int PaymentState;
    double Consumption;
    int State;
    String CreateTime;
    String ProcessTime;

    public Order(long id, int parkId, int employeeId, int memberId, String plateNumber, int type, int paymentState, double consumption, int state, String createTime, String processTime) {
        Id = id;
        ParkId = parkId;
        EmployeeId = employeeId;
        MemberId = memberId;
        PlateNumber = plateNumber;
        Type = type;
        PaymentState = paymentState;
        Consumption = consumption;
        State = state;
        CreateTime = createTime;
        ProcessTime = processTime;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public int getParkId() {
        return ParkId;
    }

    public void setParkId(int parkId) {
        ParkId = parkId;
    }

    public int getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(int employeeId) {
        EmployeeId = employeeId;
    }

    public int getMemberId() {
        return MemberId;
    }

    public void setMemberId(int memberId) {
        MemberId = memberId;
    }

    public String getPlateNumber() {
        return PlateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        PlateNumber = plateNumber;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public int getPaymentState() {
        return PaymentState;
    }

    public void setPaymentState(int paymentState) {
        PaymentState = paymentState;
    }

    public double getConsumption() {
        return Consumption;
    }

    public void setConsumption(double consumption) {
        Consumption = consumption;
    }

    public int getState() {
        return State;
    }

    public void setState(int state) {
        State = state;
    }

    public String getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(String createTime) {
        CreateTime = createTime;
    }

    public String getProcessTime() {
        return ProcessTime;
    }

    public void setProcessTime(String processTime) {
        ProcessTime = processTime;
    }
}
