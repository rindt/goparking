package com.lesmtech.goparking.fragment.boarding;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.lesmtech.goparking.HomeActivity;
import com.lesmtech.goparking.R;
import com.lesmtech.goparking.SupportInfoActivity;
import com.lesmtech.goparking.entity.Employee;
import com.lesmtech.goparking.entity.json.ErrorResponse;
import com.lesmtech.goparking.entity.json.SuccessResponse;
import com.lesmtech.goparking.tool.GRetrofit;

import java.io.IOException;
import java.util.Objects;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.jpush.android.api.JPushInterface;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

/**
 * @author Rindt
 * @version 0.1
 * @since 9/27/15
 */
public class SignInFragment extends Fragment {

    public static String TAG = "SignInFragment";

    @Bind(R.id.username)
    EditText mUserName;

    @Bind(R.id.password)
    EditText mPassword;

    @Bind(R.id.id)
    EditText mId;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_signin, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }


    public class SignInWithUsernameAndPassword extends AsyncTask<String, Void, Employee> {

        @Override
        protected Employee doInBackground(String... params) {
            try {
                Call<Employee> call = GRetrofit.getInstance().login(params[0], params[1], params[2]);
                Response<Employee> response = call.execute();
                Employee employee = response.body();
                if (employee != null) {
                    return employee;
                }
                return null;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Employee employee) {

            if (employee != null) {

                if (employee.getId() != 0) {

                    Log.d(TAG, String.valueOf(employee.getId()));
                    Log.d(TAG, String.valueOf(employee.getParkId()));

                    // Save to SharedPreference
                    SharedPreferences session = getActivity().getSharedPreferences("session", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = session.edit();
                    editor.putLong(Employee.ID, employee.getId());
                    editor.putString(Employee.JOBNUM, employee.getJobNum());
                    editor.putString(Employee.PASSWORD, employee.getPassword());
                    editor.putString(Employee.FULLNAME, employee.getFullName());
                    editor.putString(Employee.IDCARDNUM, employee.getIdCardNum());
                    editor.putInt(Employee.PARKID, employee.getParkId());
                    editor.putString(Employee.PHOTOURL, employee.getPhotoUrl());
                    editor.putString(Employee.TELPHONE, employee.getTelPhone());
                    editor.putInt(Employee.WORKINGPROPERTY, employee.getWorkingProperty());
                    editor.apply();

                    // Send RegisterId to Server
                    new RegisterJPushIdToServer()
                            .execute(String.valueOf(employee.getId()), JPushInterface.getRegistrationID(getActivity()));

                    // Start Activity
                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                } else {
                    Toast.makeText(getActivity(), employee.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }
            // NetWork Failure
        }
    }

    @OnClick(R.id.action_submit)
    public void actionSignIn() {
        new SignInWithUsernameAndPassword().execute(mUserName.getText().toString(), mId.getText().toString(), mPassword.getText().toString());
    }

    @OnClick(R.id.support)
    public void actionSupport() {
        Intent intent = new Intent(getActivity(), SupportInfoActivity.class);
        startActivity(intent);
    }

    public class RegisterJPushIdToServer extends AsyncTask<String, Void, SuccessResponse> {
        @Override
        protected SuccessResponse doInBackground(String... params) {

            String id = params[0];

            String regId = params[1];

            try {
                SuccessResponse response = GRetrofit.getInstance().postRegisterIDToServer(String.valueOf(id), regId).execute().body();
                if (response != null && response.getRowsCount() == 1) {
                    return response;
                }
            } catch (IOException io) {
                io.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(SuccessResponse successResponse) {
            if (successResponse != null) {
//                Toast.makeText(getActivity(),successResponse.getMsg() , Toast.LENGTH_SHORT).show();
            }
        }
    }

}
