package com.lesmtech.goparking.fragment.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lesmtech.goparking.CarEnterActivity;
import com.lesmtech.goparking.CarExitActivity;
import com.lesmtech.goparking.SpotReleasingActivity;
import com.lesmtech.goparking.R;
import com.lesmtech.goparking.SpotLeftActivity;
import com.lesmtech.goparking.SpotOccupiedActivity;
import com.lesmtech.goparking.SpotReservedActivity;
import com.lesmtech.goparking.entity.Employee;
import com.lesmtech.goparking.entity.PanelInfo;
import com.lesmtech.goparking.entity.json.ErrorResponse;
import com.lesmtech.goparking.tool.GRetrofit;
import com.lesmtech.goparking.view.GridItemView;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Rindt
 * @version 0.1
 * @since 9/29/15
 */
public class PanelFragment extends Fragment {

    private String uid;

    private String photoUrl;

    private boolean isFirstOpen = true;

    /**
     * Employee status, 0, 在职, 1, 离开
     */
    private int mWorkingProperty;

    private String landingTime;

    @Bind(R.id.name)
    TextView mName;

    @Bind(R.id.id)
    TextView mId;

    @Bind(R.id.state)
    TextView mState;

    @Bind(R.id.time)
    TextView mLastTimeLogIn;

    @Bind(R.id.profile_image)
    ImageView profileImage;

    @Bind(R.id.num_of_spot_occupied)
    GridItemView mSpotOccupied;

    @Bind(R.id.num_of_spot_left)
    GridItemView mSpotLeft;

    @Bind(R.id.num_of_spot_reserve)
    GridItemView mSpotReserve;

    @Bind(R.id.number_leaving)
    GridItemView mSpotLeaving;

    private String[] stateArray;

    private String parkId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        isFirstOpen = false;
        return inflater.inflate(R.layout.fragment_panel, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        getActivity().setTitle("首页");
        // Request server to render view, also update the state
        stateArray = getResources().getStringArray(R.array.state_array);

        SharedPreferences session = getActivity().getSharedPreferences("session", Context.MODE_PRIVATE);

        if (session.getInt(Employee.PARKID, -1) != -1) {
            parkId = String.valueOf(session.getInt(Employee.PARKID, -1));
            uid = String.valueOf(session.getLong(Employee.ID, -1));
            mWorkingProperty = session.getInt(Employee.WORKINGPROPERTY, -1);
            photoUrl = session.getString(Employee.PHOTOURL, null);
            landingTime = session.getString(Employee.LANDINGTIME, null);
            String id = session.getString(Employee.JOBNUM, null);
            if (id != null) {
                mId.setText("员工号:  " + id);
            }
        }

        if (photoUrl != null && !photoUrl.equals("")) {
            Picasso.with(getActivity()).load(photoUrl).resize(60, 60).into(profileImage);
        }

        if (landingTime != null) {
            mLastTimeLogIn.setText(landingTime);
        }

        new GetCarParkingInfo().execute(parkId);

    }

    @OnClick(R.id.record_parking)
    public void recordParking() {
        Intent intent = new Intent(getActivity(), CarEnterActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.leave_submit)
    public void leaveSubmit() {
        Intent intent = new Intent(getActivity(), CarExitActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.action_change_state)
    public void changeState() {

        int i ;

        for (i = 0; i < 2; i++) {
            if (mState.getText().toString().equals(stateArray[i])) {
                if (i == 1) {
                    mState.setText(stateArray[0]);
                    mState.getBackground().setLevel(0);
                    break;
                } else {
                    mState.setText(stateArray[i + 1]);
                    mState.getBackground().setLevel(i + 1);
                    break;
                }
            }
        }
        mWorkingProperty = i;
        // Request to server to update the state
        if (uid != null) {
            new EditWorkingProperty().execute(uid, String.valueOf(mWorkingProperty));
        }
    }

    public class GetCarParkingInfo extends AsyncTask<String, Void, PanelInfo> {

        @Override
        protected PanelInfo doInBackground(String... params) {

            String parkId = params[0];

            try {
                PanelInfo panelInfo = GRetrofit.getInstance().showPanelInfo(parkId).execute().body();
                if (panelInfo != null && panelInfo.getBerthcount() != 0) {
                    return panelInfo;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(PanelInfo panelInfo) {
            if (panelInfo != null) {
                mSpotOccupied.setData(panelInfo.getNumberOfOccupied(), "辆", "等待支付");
                mSpotLeft.setData(panelInfo.getNumberOfLeft(), "/" + panelInfo.getBerthcount(), "剩余车位");
                mSpotReserve.setData(panelInfo.getNumberOfReserved(), "辆", "已预约");
                mSpotLeaving.setData(panelInfo.getNumberOfLeaving(), "辆", "等待离场");
            }
        }
    }

    public class EditWorkingProperty extends AsyncTask<String, Void, ErrorResponse> {
        @Override
        protected ErrorResponse doInBackground(String... params) {

            String id = params[0];

            String WorkingProperty = params[1];

            try {
                ErrorResponse response = GRetrofit.getInstance().editWorkingState(id, WorkingProperty).execute().body();
                if (response != null && response.getRowsCount().equals("0")) {
                    return null;
                } else {
                    return response;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(ErrorResponse errorResponse) {
            if (errorResponse != null) {
                Toast.makeText(getActivity(), "状态变更成功...", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.num_of_spot_occupied)
    void showSpotOccupied() {
        Intent showSpotOccupied = new Intent(getActivity(), SpotOccupiedActivity.class);
        startActivity(showSpotOccupied);
    }


    @OnClick(R.id.number_leaving)
    void showMessageListDetail() {
        Intent intent = new Intent(getActivity(), SpotReleasingActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.num_of_spot_reserve)
    void showReservedSpot() {
        Intent intent = new Intent(getActivity(), SpotReservedActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.num_of_spot_left)
    void showLeftSpot() {
        Intent intent = new Intent(getActivity(), SpotLeftActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("PanelFragment", "Update");
        if (!isFirstOpen) {
            updateData();
        }
    }

    private void updateData() {
        new GetCarParkingInfo().execute(parkId);
    }
}
