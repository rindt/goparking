package com.lesmtech.goparking.fragment.home;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lesmtech.goparking.R;
import com.lesmtech.goparking.entity.CashRecord;
import com.lesmtech.goparking.entity.Employee;
import com.lesmtech.goparking.entity.json.SuccessResponse;
import com.lesmtech.goparking.tool.DataFormatConvert;
import com.lesmtech.goparking.tool.GRetrofit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.IllegalFormatException;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Rindt
 * @version 0.1
 * @since 11/20/15
 */
public class CashRecordFragment extends Fragment {

    public final static String TAG = CashRecordFragment.class.getName();

    @Bind(R.id.start_day)
    TextView startDay;

    @Bind(R.id.end_day)
    TextView endDay;

    @Bind(R.id.lv_result)
    ListView listView;

    @Bind(R.id.number_cash)
    TextView amountOfCash;

    boolean hasMoreData = true;

    private ArrayList<CashRecord> records;

    View selectedView;

    private String parkId;

    private String employeeId;

    private int page = 1;

    public DatePickerDialog.OnDateSetListener mOnDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Date date = (Date) selectedView.getTag();
            date.setDay(dayOfMonth);
            date.setMonth(monthOfYear);
            date.setYear(year);
            ((TextView) selectedView).setText("" + String.valueOf(monthOfYear + 1) + "月" + String.valueOf(dayOfMonth) + "日");
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cash_record, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setUpInitialValue();
        // Get parkId;
        SharedPreferences session = getActivity().getSharedPreferences("session", Context.MODE_PRIVATE);
        parkId = String.valueOf(session.getInt(Employee.PARKID, -1));
        employeeId = String.valueOf(session.getLong(Employee.ID, -1));

        // Default request today's records
        // get amount of cash
        requestCashData();
    }

    private void setUpInitialValue() {

        getActivity().setTitle("现金记录");

        records = new ArrayList<>();

        listView.setAdapter(mCashRecordsAdapter);

        Calendar c = Calendar.getInstance();

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        endDay.setTag(new Date(year, month, day));
        endDay.setText(String.valueOf(month + 1) + "月" + String.valueOf(day) + "日");

        c.add(Calendar.DAY_OF_YEAR, -1);
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        startDay.setTag(new Date(year, month, day));
        startDay.setText(String.valueOf(month + 1) + "月" + String.valueOf(day) + "日");
    }

    @OnClick(R.id.start_day)
    public void selectStartDay(View view) {
        selectedView = view;
        Date date = (Date) startDay.getTag();
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), mOnDateSetListener, date.getYear(), date.getMonth(), date.getDay());
        dialog.show();
    }

    @OnClick(R.id.end_day)
    public void selectEndDay(View view) {
        selectedView = view;
        Date date = (Date) endDay.getTag();
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), mOnDateSetListener, date.getYear(), date.getMonth(), date.getDay());
        dialog.show();
    }

    @OnClick(R.id.action_search)
    public void actionSearch() {
        records.clear();
        hasMoreData = true;
        page = 1;
        requestCashData();
    }

    public class CashAmountTask extends AsyncTask<String, Void, SuccessResponse> {
        @Override
        protected SuccessResponse doInBackground(String... params) {
            try {
                SuccessResponse response = GRetrofit.getInstance().getAmountOfCashOrder(params[0], params[1], params[2]).execute().body();
                if (response != null) {
                    return response;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(SuccessResponse successResponse) {
            if (successResponse != null) {
                amountOfCash.setText(successResponse.getMsg());
            }
        }
    }


    public class CashRecordTask extends AsyncTask<String, Void, List<CashRecord>> {
        @Override
        protected List<CashRecord> doInBackground(String... params) {

            String employeeId = params[0];

            String startTime = params[1];

            String endTime = params[2];

            try {
                List<CashRecord> response = GRetrofit.getInstance().getCashRecordList(employeeId, startTime, endTime, page++).execute().body();
                if (response != null) {
                    if(response.size() < 10){
                        hasMoreData = false;
                    }
                    return response;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (IllegalFormatException e) {
                Toast.makeText(getActivity(), "结果不存在...", Toast.LENGTH_SHORT).show();
            } catch (IllegalStateException e) {
                Log.d("app", "IllegalStateException");
                // UnExcepted Data
                hasMoreData = false;
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<CashRecord> cashRecords) {
            if (cashRecords == null) {
                Toast.makeText(getActivity(), "此时段无数据...", Toast.LENGTH_SHORT).show();
            } else {
                // Render View.
                records.addAll(cashRecords);
                mCashRecordsAdapter.notifyDataSetChanged();
                Toast.makeText(getActivity(), "查询成功...", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public BaseAdapter mCashRecordsAdapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return records.size();
        }

        @Override
        public Object getItem(int position) {
            return records.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.list_item_cash_record, parent, false);
            }

            final TextView plateNumber = (TextView) convertView.findViewById(R.id.plateNumber);
            final TextView time = (TextView) convertView.findViewById(R.id.time_of_paying);
            final TextView amount = (TextView) convertView.findViewById(R.id.amount);

            CashRecord record = records.get(position);

            time.setText(DataFormatConvert.uglyDataToMill(record.getCreateTime()));
            amount.setText(String.valueOf(record.getAmount()));
            plateNumber.setText(record.getPlateNumber());

            // Page nation
            if (hasMoreData && position + 5 > records.size()) {
                requestCashData();
            }
            return convertView;
        }
    };

    private void requestCashData() {

        Date startDayTag = (Date) startDay.getTag();
        Date endDayTag = (Date) endDay.getTag();

        new CashAmountTask().execute(employeeId
                , startDayTag.toString(), endDayTag.toString());

        new CashRecordTask().execute(employeeId
                , startDayTag.toString(), endDayTag.toString());
    }

    /**
     * Date
     */
    public class Date {

        int day;
        int month;
        int year;

        public Date(int year, int month, int day) {
            this.year = year;
            this.month = month;
            this.day = day;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public int getDay() {
            return day;
        }

        public void setDay(int day) {
            this.day = day;
        }

        public int getMonth() {
            return month;
        }

        public void setMonth(int month) {
            this.month = month;
        }

        public String toString() {
            StringBuilder builder = new StringBuilder();
            builder.append(this.getYear()).append("-").append(this.getMonth() + 1).append("-").append(this.getDay());
            return builder.toString();
        }
    }
}
