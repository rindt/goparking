package com.lesmtech.goparking.fragment.home;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.lesmtech.goparking.R;
import com.lesmtech.goparking.entity.Employee;
import com.lesmtech.goparking.entity.StatisticsResponse;
import com.lesmtech.goparking.entity.json.SuccessResponse;
import com.lesmtech.goparking.tool.GRetrofit;

import java.io.IOException;
import java.util.Calendar;
import java.util.IllegalFormatException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Rindt
 * @version 0.1
 * @since 10/14/15
 */
public class StatHistFragment extends Fragment {

    public final static String TAG = StatHistFragment.class.getName();

    @Bind(R.id.start_day)
    TextView startDay;

    @Bind(R.id.end_day)
    TextView endDay;

    @Bind(R.id.total_money)
    TextView totalMoney;

    @Bind(R.id.total_number)
    TextView totalNumber;

    @Bind(R.id.total_reserved)
    TextView totalReserved;

    View selectedView;

    private String parkId;

    public DatePickerDialog.OnDateSetListener mOnDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Date date = (Date) selectedView.getTag();
            date.setDay(dayOfMonth);
            date.setMonth(monthOfYear);
            date.setYear(year);
            ((TextView) selectedView).setText("" + String.valueOf(monthOfYear + 1) + "月" + String.valueOf(dayOfMonth) + "日");
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_stat_hist, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setUpInitialValue();
        // Get parkId;
        SharedPreferences session = getActivity().getSharedPreferences("session", Context.MODE_PRIVATE);
        parkId = String.valueOf(session.getInt(Employee.PARKID, -1));
    }

    private void setUpInitialValue() {

        getActivity().setTitle("历史统计");

        Calendar c = Calendar.getInstance();

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        endDay.setTag(new Date(year, month, day));
        endDay.setText(String.valueOf(month + 1) + "月" + String.valueOf(day) + "日");

        c.add(Calendar.DAY_OF_YEAR, -1);
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        startDay.setTag(new Date(year, month, day));
        startDay.setText(String.valueOf(month + 1) + "月" + String.valueOf(day) + "日");
    }

    @OnClick(R.id.start_day)
    public void selectStartDay(View view) {
        selectedView = view;
        Date date = (Date) startDay.getTag();
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), mOnDateSetListener, date.getYear(), date.getMonth(), date.getDay());
        dialog.show();
    }

    @OnClick(R.id.end_day)
    public void selectEndDay(View view) {
        selectedView = view;
        Date date = (Date) endDay.getTag();
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), mOnDateSetListener, date.getYear(), date.getMonth(), date.getDay());
        dialog.show();
    }

    @OnClick(R.id.action_search)
    public void actionSearch() {
        Date start = (Date) startDay.getTag();
        Date end = (Date) endDay.getTag();
        if (parkId != null) {
            new StatHisTask().execute(
                    (start.getYear() + "-" + (start.getMonth() + 1) + "-" + start.getDay()),
                    end.getYear() + "-" + (end.getMonth() + 1) + "-" + end.getDay(), parkId);
        }
    }

    public class Date {

        int day;
        int month;
        int year;

        public Date(int year, int month, int day) {
            this.year = year;
            this.month = month;
            this.day = day;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public int getDay() {
            return day;
        }

        public void setDay(int day) {
            this.day = day;
        }

        public int getMonth() {
            return month;
        }

        public void setMonth(int month) {
            this.month = month;
        }
    }

    public class StatHisTask extends AsyncTask<String, Void, StatisticsResponse> {
        @Override
        protected StatisticsResponse doInBackground(String... params) {

            String startTime = params[0];
            String endTime = params[1];
            String parkId = params[2];

            try {
                StatisticsResponse response = GRetrofit.getInstance().getHisReport(startTime, endTime, parkId).execute().body();
                if (response != null) {
                    return response;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (IllegalFormatException e) {
                Toast.makeText(getActivity(), "结果不存在...", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(StatisticsResponse statisticsResponse) {
            if (statisticsResponse == null) {
                Toast.makeText(getActivity(), "查询失败...", Toast.LENGTH_SHORT).show();
            } else {
                // Render View.
                totalMoney.setText("" + statisticsResponse.getRevenue());
                totalReserved.setText("" + statisticsResponse.getReserveAmount());
                totalNumber.setText("" + statisticsResponse.getCarAmount());
                Toast.makeText(getActivity(), "查询成功...", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

