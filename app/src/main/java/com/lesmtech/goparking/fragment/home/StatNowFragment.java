package com.lesmtech.goparking.fragment.home;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.lesmtech.goparking.R;
import com.lesmtech.goparking.entity.Employee;
import com.lesmtech.goparking.entity.StatisticsResponse;
import com.lesmtech.goparking.tool.GRetrofit;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Rindt
 * @version 0.1
 * @since 10/14/15
 */
public class StatNowFragment extends Fragment {

    @Bind(R.id.gain)
    TextView gainAmount;

    @Bind(R.id.reserveAmount)
    TextView reserveAmount;

    @Bind(R.id.parkingAmount)
    TextView parkingAmount;

    private int parkId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_stat_now, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        getActivity().setTitle("今日统计");
        // Request data
        SharedPreferences session = getActivity().getSharedPreferences("session", Context.MODE_PRIVATE);
        parkId = session.getInt(Employee.PARKID, -1);
        if (parkId != -1) {
            new GetTodayReport().execute();
        }
    }

    public class GetTodayReport extends AsyncTask<String, Void, StatisticsResponse> {
        @Override
        protected StatisticsResponse doInBackground(String... params) {
            try {
                StatisticsResponse todayReport = GRetrofit.getInstance().getTodayReport(parkId).execute().body();
                return todayReport;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(StatisticsResponse statisticsToday) {
            if(statisticsToday == null){
                Toast.makeText(getActivity(), "报告不可用", Toast.LENGTH_SHORT).show();
            }
            else{
                gainAmount.setText(String.valueOf(statisticsToday.getRevenue()));
                parkingAmount.setText(String.valueOf(statisticsToday.getCarAmount()));
                reserveAmount.setText(String.valueOf(statisticsToday.getReserveAmount()));
            }
        }
    }

    @OnClick(R.id.refresh)
    public void actionRefresh(){
        new GetTodayReport().execute();
    }
}

