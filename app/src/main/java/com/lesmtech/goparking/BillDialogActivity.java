package com.lesmtech.goparking;

import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lesmtech.goparking.asyntask.CashPayTask;
import com.lesmtech.goparking.asyntask.GetOrderInfoTask;
import com.lesmtech.goparking.asyntask.SpotReleasingTask;
import com.lesmtech.goparking.entity.Employee;
import com.lesmtech.goparking.entity.Order;
import com.lesmtech.goparking.entity.RepayObject;
import com.lesmtech.goparking.entity.json.SuccessResponse;
import com.lesmtech.goparking.tool.DataFormatConvert;
import com.lesmtech.goparking.tool.GRetrofit;
import com.lesmtech.goparking.view.StatusView;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.jpush.android.api.InstrumentedActivity;

/**
 * @author Rindt
 * @version 0.1
 * @since 10/1/15
 */
public class BillDialogActivity extends InstrumentedActivity {

    public final static int RESULT_OK_PAY_NOT_LEAVE = 8000;

    public final static int RESULT_OK_PAY_LEAVE = 8001;

    public final static String TAG = "BillDialogActivity";

    public static String BILL_TYPE = "type";

    public static int BILL_TYPE_OVERTIME = 1;

    public static int BILL_TYPE_LEAVING = 0;

    public static boolean isForeground = false;

    public final static String RECORD_ID = "RECORD_id";

    public final static String EMPLOYEE_ID = "employee_id";

    private String employeeId;

    private String recordId;

    private Order order;

    @Bind(R.id.title)
    TextView title;

    @Bind(R.id.duration)
    TextView duration;

    @Bind(R.id.consumption)
    TextView consupmtion;

    @Bind(R.id.plate)
    TextView plateNumber;

    @Bind(R.id.paidStatusView)
    StatusView paidView;

    @Bind(R.id.unpaidStatusView)
    StatusView unpaidView;

    @Bind(R.id.leave_confirm_dialog)
    LinearLayout mConfirmLeaveDialog;

    @Bind(R.id.bill_container)
    LinearLayout billContainer;

    @Bind(R.id.qrImage)
    ImageView qrImage;

    StringBuilder qrImageUrl = new StringBuilder(GRetrofit.BASEURL);

    //for receive customer msg from jpush server
    private MessageReceiver mMessageReceiver;
    public static final String MESSAGE_RECEIVED_ACTION = "com.example.jpushdemo.MESSAGE_RECEIVED_ACTION";
    public static final String KEY_TITLE = "title";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_EXTRAS = "extras";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        recordId = intent.getStringExtra(RECORD_ID);
        employeeId = intent.getStringExtra(EMPLOYEE_ID);

        qrImageUrl.append("/Manage/QRCode?OrderInfoid=").append(recordId);

        Picasso.with(this).load(qrImageUrl.toString()).resize(100, 100).centerInside().into(qrImage);

        registerMessageReceiver();

        if (getIntent().getIntExtra(BILL_TYPE, 0) == 0) {

        } else {
            // 1... change a little bit UI
            title.setTextColor(getResources().getColor(R.color.red));
            title.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.small_warning), null, null, null);
            title.setText("超时账单");
        }
        // request server to get payment info
        requestOrderInfo();
    }

    @OnClick(R.id.action_submit)
    void actionSubmit() {
        // Using QR Code to pay
        // Send back
        Intent returnIntent = new Intent();
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @OnClick(R.id.action_cash)
    void actionCash() {
        // Pay by cash, request to server
        payByCash();
    }

    private void registerMessageReceiver() {
        mMessageReceiver = new MessageReceiver();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        filter.addAction(MESSAGE_RECEIVED_ACTION);
        registerReceiver(mMessageReceiver, filter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isForeground = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isForeground = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mMessageReceiver);
    }

    public class MessageReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (MESSAGE_RECEIVED_ACTION.equals(intent.getAction())) {
                String messge = intent.getStringExtra(KEY_MESSAGE);
                String extras = intent.getStringExtra(KEY_EXTRAS);
                StringBuilder showMsg = new StringBuilder();
                showMsg.append(KEY_MESSAGE + " : " + messge + "\n");
                if (extras != null && !extras.equals("")) {
                    showMsg.append(KEY_EXTRAS + " : " + extras + "\n");
                }
                displayToast(showMsg.toString());
            }
        }
    }

    private void requestOrderInfo() {
        GetOrderInfoTask task = new GetOrderInfoTask(employeeId, recordId);
        task.setmGetOrderInfoTaskListener(new GetOrderInfoTask.GetOrderInfoTaskListener() {
            @Override
            public void successOrder(Order order) {
                Log.d("SUCCESS", "获得订单信息.");
                BillDialogActivity.this.order = order;
                plateNumber.setText(order.getPlateNumber());
                duration.setText(DataFormatConvert.uglyDataToDuration(order.getProcessTime()));
                consupmtion.setText(BillDialogActivity.this.getResources().getString(R.string.rmb) + String.valueOf(order.getConsumption()));
            }

            @Override
            public void failureOrder(String msg) {
                Log.d("FAILURE", "未能获取订单信息.");
            }
        });
        task.execute();
    }

    private void displayToast(String content) {
        Toast.makeText(BillDialogActivity.this, content, Toast.LENGTH_SHORT).show();
    }

    private void payByCash() {
        if (order != null) {
            CashPayTask task = new CashPayTask(employeeId, String.valueOf(order.getId()), recordId);
            task.setmCashPayTaskListener(new CashPayTask.CashPayTaskListener() {
                @Override
                public void paySuccess(SuccessResponse response) {
                    displayToast("支付成功...");
                    // Pay success by cash.. see whether leave the parking lot.
                    // Change pay status.
                    changePaymentStatus(true);
                    showConfirmLeaveDialog();
                }

                @Override
                public void payFailure() {
                    Log.d(TAG, "Payment Failure");
                    displayToast("支付失败..请联系管理员..");
                }
            });
            task.execute();
        }
    }

    private void showConfirmLeaveDialog() {
        ValueAnimator va = ValueAnimator.ofInt(0, billContainer.getHeight() / 2);
        int duration = 1500;
        va.setDuration(duration);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                mConfirmLeaveDialog.getLayoutParams().height = value.intValue();
                mConfirmLeaveDialog.requestLayout();
            }
        });
        va.start();
    }

    private void changePaymentStatus(boolean isPaid) {
        paidView.setChecked(isPaid);
        unpaidView.setChecked(!isPaid);
    }


    @OnClick(R.id.action_cancel)
    void actionCancel() {
        Intent returnIntent = new Intent();
        setResult(RESULT_OK_PAY_NOT_LEAVE, returnIntent);
        finish();
    }


    @OnClick(R.id.action_leave)
    void actionLeave() {
        // The car is leaving, so we confirm
        SpotReleasingTask task = new SpotReleasingTask(recordId, employeeId);
        task.setmSpotReleasingTaskListener(new SpotReleasingTask.SpotReleasingTaskListener() {
            @Override
            public void successReleasing() {
                Intent returnIntent = new Intent();
                setResult(RESULT_OK_PAY_LEAVE, returnIntent);
                finish();
            }
            @Override
            public void needPayBaby(RepayObject object) {
                // Should I handle?
            }

            @Override
            public void hasReleased() {

            }
        });
        task.execute();
    }

}
