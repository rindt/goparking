package com.lesmtech.goparking.asyntask;

import android.os.AsyncTask;

import com.lesmtech.goparking.entity.Order;
import com.lesmtech.goparking.tool.GRetrofit;

import java.io.IOException;

/**
 * @author Rindt
 * @version 0.1
 * @since 11/22/15
 */
public class GetOrderInfoTask extends AsyncTask<Void, Void, Order> {

    private String employeeId;

    private String carParkingId;

    private GetOrderInfoTaskListener mGetOrderInfoTaskListener;

    public interface GetOrderInfoTaskListener {
        void successOrder(Order order);

        void failureOrder(String msg);
    }

    public void setmGetOrderInfoTaskListener(GetOrderInfoTaskListener mGetOrderInfoTaskListener) {
        this.mGetOrderInfoTaskListener = mGetOrderInfoTaskListener;
    }

    public GetOrderInfoTask(String employeeId, String carParkingId) {
        this.employeeId = employeeId;
        this.carParkingId = carParkingId;
    }

    @Override
    protected Order doInBackground(Void... params) {
        try {
            Order order = GRetrofit.getInstance().getOrderInfo(employeeId, carParkingId).execute().body();
            if (order != null) {
                return order;
            }
        } catch (IllegalStateException e) {
            if(mGetOrderInfoTaskListener != null) {
                mGetOrderInfoTaskListener.failureOrder("返回数据非法...");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Order order) {
        if (mGetOrderInfoTaskListener != null) {
            if (order != null) {
                mGetOrderInfoTaskListener.successOrder(order);
            } else {
                mGetOrderInfoTaskListener.failureOrder("无法获取订单信息...");
            }
        }

    }
}
