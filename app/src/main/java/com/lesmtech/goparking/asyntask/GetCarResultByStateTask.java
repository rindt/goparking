package com.lesmtech.goparking.asyntask;

import android.os.AsyncTask;
import android.util.Log;

import com.lesmtech.goparking.entity.Berth;
import com.lesmtech.goparking.tool.GRetrofit;

import java.io.IOException;
import java.util.List;

/**
 * @author Rindt
 * @version 0.1
 * @since 11/18/15
 */
public class GetCarResultByStateTask extends AsyncTask<String, Void, List<Berth>> {

    public boolean hasMoreData = true;

    public GetCarResultTaskListener mListener;

    public void setmListener(GetCarResultTaskListener mListener) {
        this.mListener = mListener;
    }

    public interface GetCarResultTaskListener {
        void success(int state, boolean hasMoreData, List<Berth> berths);

        void failure();
    }

    @Override
    protected List<Berth> doInBackground(String... params) {

        String parkid = params[0];
        int page = Integer.valueOf(params[1]);
        String state = String.valueOf(params[2]);

        try {
            List<Berth> berths = GRetrofit.getInstance().showCarParkingListByState(parkid, state, page).execute().body();
            if (berths.size() < 10) {
                hasMoreData = false;
            }
            if (berths.size() != 0) {
                return berths;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            Log.d("app", "No more data");
            hasMoreData = false;
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<Berth> berths) {
        if (berths != null) {

        }
    }
}
