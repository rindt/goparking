package com.lesmtech.goparking.asyntask;

import android.os.AsyncTask;

import com.lesmtech.goparking.entity.RepayObject;
import com.lesmtech.goparking.entity.json.SuccessResponse;
import com.lesmtech.goparking.tool.GRetrofit;

import java.io.IOException;
import java.util.IllegalFormatException;

/**
 * @author Rindt
 * @version 0.1
 * @since 12/14/15
 */
public class SpotReleasingTask extends AsyncTask<Void, Void, RepayObject> {

    private String id;

    private String employeeId;

    private SpotReleasingTaskListener mSpotReleasingTaskListener;

    public void setmSpotReleasingTaskListener(SpotReleasingTaskListener mSpotReleasingTaskListener) {
        this.mSpotReleasingTaskListener = mSpotReleasingTaskListener;
    }

    public interface SpotReleasingTaskListener {
        void successReleasing();

        void needPayBaby(RepayObject object);

        void hasReleased();
    }

    public SpotReleasingTask(String id, String employeeId) {
        this.id = id;
        this.employeeId = employeeId;
    }

    @Override
    protected RepayObject doInBackground(Void... params) {
        try {
            RepayObject object = GRetrofit.getInstance().confirmBerthRelease(id, employeeId).execute().body();
            if (object != null && object.getId() != null) {
                return object;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalFormatException e) {
            e.printStackTrace();
            // Release success
            return null;
        }
        return null;
    }

    @Override
    protected void onPostExecute(RepayObject repayObject) {
        if (mSpotReleasingTaskListener != null) {
            if(repayObject != null && repayObject.getId() != null){
                mSpotReleasingTaskListener.needPayBaby(repayObject);
            } else {
                mSpotReleasingTaskListener.successReleasing();
            }
        }
    }
}
