package com.lesmtech.goparking.asyntask;

import android.os.AsyncTask;
import android.widget.Toast;

import com.lesmtech.goparking.Manifest;
import com.lesmtech.goparking.entity.Berth;
import com.lesmtech.goparking.tool.GRetrofit;

import java.io.IOException;
import java.util.IllegalFormatException;

/**
 * @author Rindt
 * @version 0.1
 * @since 12/20/15
 */
public class CarEnterTask extends AsyncTask<Void, Void, Berth> {

    private String parkId;

    private String plateNumber;

    private String employeeId;

    private CarEnterTaskListerner mCarEnterTaskListerner;

    public void setmCarEnterTaskListerner(CarEnterTaskListerner mCarEnterTaskListerner) {
        this.mCarEnterTaskListerner = mCarEnterTaskListerner;
    }

    public interface CarEnterTaskListerner {
        void successEnterCar(Berth berth);

        void failureEnterCar();
    }

    public CarEnterTask(String parkId, String plateNumber, String employeeId) {
        this.parkId = parkId;
        this.plateNumber = plateNumber;
        this.employeeId = employeeId;
    }

    @Override
    protected Berth doInBackground(Void... params) {

        try {
            Berth response = GRetrofit.getInstance().addCarParkingInfo(parkId, plateNumber, employeeId).execute().body();
            if (response != null && response.getId() != 0) {
                return response;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalFormatException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }


    @Override
    protected void onPostExecute(Berth berth) {
        if (mCarEnterTaskListerner != null) {
            if (berth != null) {
                mCarEnterTaskListerner.successEnterCar(berth);
            } else {
                mCarEnterTaskListerner.failureEnterCar();
            }
        }
    }
}
