package com.lesmtech.goparking.asyntask;

import android.os.AsyncTask;
import android.util.Log;

import com.lesmtech.goparking.entity.Berth;
import com.lesmtech.goparking.tool.GRetrofit;

import java.io.IOException;
import java.util.List;

/**
 * @author Rindt
 * @version 0.1
 * @see com.lesmtech.goparking.SpotReleasingActivity
 * @see com.lesmtech.goparking.SpotOccupiedActivity
 * @since 11/29/15
 */
public class GetCarParkingListByState extends AsyncTask<String, Void, List<Berth>> {

    private GetCarParkingListByStateListener mGetCarParkingListByStateListener;

    public void setmGetCarParkingListByStateListener(GetCarParkingListByStateListener mGetCarParkingListByStateListener) {
        this.mGetCarParkingListByStateListener = mGetCarParkingListByStateListener;
    }

    public interface GetCarParkingListByStateListener {
        void success(List<Berth> berths, boolean hasMoreData);
        void failure();
    }

    @Override
    protected List<Berth> doInBackground(String... params) {

        String parkid = params[0];
        String state = params[1];
        int page = Integer.valueOf(params[2]);

        try {
            List<Berth> berths = GRetrofit.getInstance().getCarParkingListP(parkid, state, page).execute().body();
            if (berths != null && berths.size() != 0) {
                return berths;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            Log.d("app", "No more data");
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<Berth> berths) {
        super.onPostExecute(berths);
        if(berths != null){
            mGetCarParkingListByStateListener.success(berths, berths.size() == 20);
        }
        else{
            mGetCarParkingListByStateListener.failure();
        }
    }
}
