package com.lesmtech.goparking.asyntask;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;

import com.lesmtech.goparking.entity.json.SuccessResponse;
import com.lesmtech.goparking.tool.GRetrofit;

import java.io.IOException;

/**
 * @author Rindt
 * @version 0.1
 * @since 11/22/15
 */
public class CashPayTask extends AsyncTask<Void, Void, SuccessResponse> {

    public interface CashPayTaskListener {

        void paySuccess(SuccessResponse response);

        void payFailure();
    }

    private String employeeId;

    private String orderInfoId;

    private String carParkingId;

    public CashPayTask(String employeeId, String orderInfoId, String carParkingId) {
        this.employeeId = employeeId;
        this.orderInfoId = orderInfoId;
        this.carParkingId = carParkingId;
    }

    private CashPayTaskListener mCashPayTaskListener;

    public void setmCashPayTaskListener(CashPayTaskListener mCashPayTaskListener) {
        this.mCashPayTaskListener = mCashPayTaskListener;
    }

    @Override
    protected SuccessResponse doInBackground(Void... params) {
        try {

            // "CarParking": "OK:修改成功",
            // "AddCashCharges": "OK:添加成功"
            // Here success response is the failure response

            SuccessResponse response = GRetrofit.getInstance().finishOrderByCash(employeeId, orderInfoId, carParkingId).execute().body();
            if (response != null && response.getRowsCount() != 0) {
                return response;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(SuccessResponse successResponse) {
        if (mCashPayTaskListener != null) {
            if (successResponse != null) {
                mCashPayTaskListener.paySuccess(successResponse);
            } else {
                mCashPayTaskListener.payFailure();
            }
        }

    }
}
