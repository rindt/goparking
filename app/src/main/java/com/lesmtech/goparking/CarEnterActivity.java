package com.lesmtech.goparking;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.lesmtech.goparking.entity.Berth;
import com.lesmtech.goparking.entity.Employee;
import com.lesmtech.goparking.entity.json.SuccessResponse;
import com.lesmtech.goparking.tool.GRetrofit;
import com.lesmtech.goparking.view.CWordKeyBoardView;
import com.lesmtech.goparking.view.CarEnterDialogView;
import com.lesmtech.goparking.view.ConfirmLeftDialogView;
import com.lesmtech.goparking.view.EWordKeyBoardView;
import com.lesmtech.goparking.view.KeyBoardView;
import com.lesmtech.goparking.view.SpotAvailableDialogView;
import com.wefika.flowlayout.FlowLayout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;

/**
 * @author Rindt
 * @version 0.1
 * @since 9/30/15
 */
public class CarEnterActivity extends AppCompatActivity implements CarEnterDialogView.CarEnterDialogListener {

    public final static String TAG = CarEnterActivity.class.getClass().getSimpleName();

    private int parkId;

    private String employeeId;

    public final static int CODE = 1;

    public final static int PLACE = 0;

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.dialog)
    FlowLayout dialog;

    @Bind(R.id.place)
    TextView place;

    @Bind(R.id.code)
    TextView code;

    @Bind(R.id.number_keyboard)
    KeyBoardView numberKeyBoard;

    @Bind(R.id.eword_keyboard)
    EWordKeyBoardView ewordKeyBoard;

    @Bind(R.id.cword_keyboard)
    CWordKeyBoardView cwordKeyBoard;

    @Bind({R.id.number_one, R.id.number_two, R.id.number_three, R.id.number_four, R.id.number_five})
    List<TextView> plateNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carenter);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        mToolbar.setTitle(getResources().getString(R.string.title_car_exit));
        mToolbar.setNavigationIcon(R.drawable.arrow_back);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));

        SharedPreferences session = getSharedPreferences("session", MODE_PRIVATE);
        parkId = session.getInt(Employee.PARKID, -1);
        employeeId = String.valueOf(session.getLong(Employee.ID, -1));

    }

    @OnClick(R.id.place)
    public void selectPlace(final View view) {

        if (ewordKeyBoard.getVisibility() == View.VISIBLE) {
            ewordKeyBoard.hideMySelf();
        }

        if (numberKeyBoard.getVisibility() == View.VISIBLE) {
            numberKeyBoard.hideMySelf();
        }

        String content = ((TextView) view).getText().toString();
        cwordKeyBoard.pinTheArrowToTheView(view, content);
        cwordKeyBoard.setOnItemClickListener(new CWordKeyBoardView.OnItemClickListener() {
            @Override
            public void theSelectedItem(String content) {
                ((TextView) view).setText(content);
            }
        });
    }


    @OnClick(R.id.code)
    public void selectCode(final View view) {

        if (cwordKeyBoard.getVisibility() == View.VISIBLE) {
            cwordKeyBoard.hideMySelf();
        }

        int index = ((TextView) view).getText().toString().charAt(0);
        ewordKeyBoard.pinTheArrowToTheView(view, index >= 65 && index <= 90 ? index - 65 : 0);
        ewordKeyBoard.setOnItemClickListener(new EWordKeyBoardView.OnItemClickListener() {
            @Override
            public void theSelectedItem(String content) {
                ((TextView) view).setText(content);
            }
        });
    }

    @OnClick({R.id.number_one, R.id.number_two, R.id.number_three, R.id.number_four, R.id.number_five})
    void onTouchNumber(final View view) {

        if (cwordKeyBoard.getVisibility() == View.VISIBLE) {
            cwordKeyBoard.hideMySelf();
        }

        String content = ((TextView) view).getText().toString();

        try {
            int number = Integer.parseInt(content);
            numberKeyBoard.pinTheArrowToTheView(view, number);
            ewordKeyBoard.pinTheArrowToTheView(view, -1);

        } catch (NumberFormatException e) {
            numberKeyBoard.pinTheArrowToTheView(view, -1);

            int index = ((TextView) view).getText().toString().charAt(0);
            ewordKeyBoard.pinTheArrowToTheView(view, index >= 65 && index <= 90 ? index - 65 : 0);
        }

        // Listener have to set up for both
        ewordKeyBoard.setOnItemClickListener(new EWordKeyBoardView.OnItemClickListener() {
            @Override
            public void theSelectedItem(String content) {
                ((TextView) view).setText(content);
                numberKeyBoard.hideMySelf();
            }
        });

        numberKeyBoard.setOnItemClickListener(new KeyBoardView.OnItemClickListener() {
            @Override
            public void theSelectedItem(String content) {
                ((TextView) view).setText(content);
                ewordKeyBoard.hideMySelf();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.action_submit)
    void actionSubmit() {
        // loading panel
        if (parkId != 0) {
            new AddCarInfo().execute(String.valueOf(parkId), getPlateNumber(), employeeId);
        }
    }

    @OnClick(R.id.action_cancel)
    void actionCancel() {
        finish();
    }

    private String getPlateNumber() {
        StringBuilder plateNumber = new StringBuilder();
        plateNumber.append(place.getText().toString());
        plateNumber.append(code.getText().toString());
        for (TextView unit : this.plateNumber) {
            plateNumber.append(unit.getText().toString());
        }
        return plateNumber.toString();
    }

    public class AddCarInfo extends AsyncTask<String, Void, Berth> {
        @Override
        protected Berth doInBackground(String... params) {

            String parkid = params[0];

            String plateNumber = params[1];

            String employeeid = params[2];

            try {
                Berth response = GRetrofit.getInstance().addCarParkingInfo(parkid, plateNumber, employeeid).execute().body();
                if (response != null && response.getId() != 0) {
                    return response;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (IllegalFormatException e){
                e.printStackTrace();
                return null;
            } catch(IllegalStateException e){
                e.printStackTrace();
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Berth berth) {
            if (berth != null) {
                // Success
                Toast.makeText(CarEnterActivity.this, String.valueOf(berth.getId()), Toast.LENGTH_SHORT).show();
                showConfirmDialog(berth);
            } else {
                // Fail
                Toast.makeText(CarEnterActivity.this, "没有录用成功..泊位错误", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showConfirmDialog(Berth mBerth) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        CarEnterDialogView view = new CarEnterDialogView(CarEnterActivity.this, mBerth);
        view.setmCarEnterDialogListener(this);
        final AlertDialog mAlertDialog;
        mAlertDialog = mBuilder.setView(view).create();
        view.setDialog(mAlertDialog);
        WindowManager.LayoutParams wmlp = mAlertDialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER;
        mAlertDialog.show();
    }

    // Show change Spot Dialog after click "Edit Spot" in Confirm dialog
    private void showSpotAvailableDialog(Berth mBerth) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        SpotAvailableDialogView view = new SpotAvailableDialogView(this, mBerth);
        view.setmDialogViewResultListener(new SpotAvailableDialogView.DialogViewResultListener() {
            @Override
            public void success(String selectedSpotNumber) {
                /////
                Log.d(TAG, " SpotAvailable CallBack: "+ selectedSpotNumber);
            }

            @Override
            public void fail() {
                //////

            }
        });
        final AlertDialog mAlertDialog;
        mAlertDialog = mBuilder.setView(view).create();
        view.setDialog(mAlertDialog);
        WindowManager.LayoutParams wmlp = mAlertDialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER;
        mAlertDialog.show();
    }

    @Override
    public void edit(Berth berth) {
        Log.d(TAG, "Edit Berth.");
        showSpotAvailableDialog(berth);
    }

    @Override
    public void cancel() {
        Log.d(TAG, "Not suppose to edit berth auto assigned.");
        finish();
    }

}
