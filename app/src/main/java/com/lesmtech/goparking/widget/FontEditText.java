package com.lesmtech.goparking.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.util.LruCache;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;

import com.lesmtech.goparking.R;


public class FontEditText extends android.widget.EditText {
    /**
     * The name of the font file
     */
    private String mFontName = "";

    /**
     * The {@link LruCache} that stores the font for
     * future use
     */
    //TODO This may be waaaay too big for just font files.
    private static final LruCache<String, Typeface> sFontCache = new
            LruCache<String, Typeface>(1024 * 1024); //10MB

    /**
     * @param context
     * @see android.widget.TextView#TextView(Context)
     */
    public FontEditText(Context context) {
        super(context);
    }

    /**
     * @param context
     * @param attrs
     * @see android.widget.TextView#TextView(Context, AttributeSet)
     */
    public FontEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupFont(context, attrs, 0, 0);
    }

    /**
     * @param context
     * @param attrs
     * @param defStyleAttr
     * @see android.widget.TextView#TextView(Context, AttributeSet, int)
     */
    public FontEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupFont(context, attrs, defStyleAttr, 0);
    }

    /**
     * @param context
     * @param attrs
     * @param defStyleAttr
     * @param defStyleRes
     * @see android.widget.TextView#TextView(Context, AttributeSet, int, int)
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FontEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    /**
     * This function reads the font from the XML definition and initializes
     * the TextView with the proper typeface.  Only .otf and .ttf files are
     * currently supported.  The file must be stored in the assets folder
     *
     * @param context
     * @param attrs
     * @param defStyleAttr
     * @param defStyleRes
     */
    private void setupFont(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        TypedArray array = context.getTheme().obtainStyledAttributes(attrs, R.styleable.FontTextView, defStyleAttr, defStyleRes);
        mFontName = array.getString(R.styleable.FontTextView_font);
        if (!TextUtils.isEmpty(mFontName)) {
            if (mFontName.trim().toLowerCase().matches(".*\\.(otf|ttf)$")) {
                //if the font file contains a .otf or .ttf extension,
                // load it as is
                Typeface font = sFontCache.get(mFontName.substring(0, mFontName.lastIndexOf('.')));
                if (font == null) {
                    font = Typeface.createFromAsset(context.getAssets(), mFontName.trim());
                    if (font == null) {
                        return;
                    } else {
                        sFontCache.put(mFontName.substring(0, mFontName.lastIndexOf('.')), font);
                    }
                }
                setTypeface(font);
            } else if (mFontName.trim().toLowerCase().matches(".*\\.(.*)$")) {
                // if the file name contains an extension that is not .otf or
                // .ttf, log an error
                Log.e(FontEditText.class.getSimpleName(), "Only .otf and .ttf files are currently supported");
            } else {
                // if no file extension is given, look for the file as either
                // .otf or .ttf
                Typeface font = sFontCache.get(mFontName);
                if (font == null) {
                    try {
                        font = Typeface.createFromAsset(context.getAssets(), mFontName.trim() + ".otf");
                    } catch (Exception e) {
                        //do nothing
                    }
                    if (font == null) {
                        try {
                            font = Typeface.createFromAsset(context.getAssets(), mFontName.trim() + ".ttf");
                        } catch (Exception e) {
                            //do nothing
                        }
                        if (font == null) {
                            return;
                        } else {
                            sFontCache.put(mFontName.trim(), font);
                        }
                    } else {
                        sFontCache.put(mFontName.trim(), font);
                    }
                }
                setTypeface(font);
            }
        }
    }
}
