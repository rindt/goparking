package com.lesmtech.goparking.service;

import com.lesmtech.goparking.entity.Berth;
import com.lesmtech.goparking.entity.CashRecord;
import com.lesmtech.goparking.entity.Employee;
import com.lesmtech.goparking.entity.Order;
import com.lesmtech.goparking.entity.PanelInfo;
import com.lesmtech.goparking.entity.ParkInfo;
import com.lesmtech.goparking.entity.RepayObject;
import com.lesmtech.goparking.entity.StatisticsResponse;
import com.lesmtech.goparking.entity.json.ErrorResponse;
import com.lesmtech.goparking.entity.json.SuccessResponse;

import java.util.List;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * @author Rindt
 * @version 0.1
 * @since 9/27/15
 */
public interface GoParkingService {

    // ------------------------- Employee Section -----------------------//

    @FormUrlEncoded
    @POST("Manage/UpdateEmployeeInfoTime")
    Call<Employee> updateEmployeeInfoTime(@Field("id") String id);

//    http://101.200.192.56/Manage/UpdateEmployeeInfoTime?id=5

    /**
     * RegisterId from JPush
     *
     * @param id              = employee id
     * @param registerationid = registerid from Jpush
     * @return
     */
    @FormUrlEncoded
    @POST("Employee/UpdateRegistrationId")
    Call<SuccessResponse> postRegisterIDToServer(@Field("id") String id, @Field("registrationid") String registerationid);


    // Change Password
    // http://101.200.192.56/Manage/EditEmployeeInfo
    @FormUrlEncoded
    @POST("/Manage/EditEmployeeInfo")
    Call<SuccessResponse> changePassword(@Field("id") String id, @Field("password") String password);

    /**
     * Work
     *
     * @param jobNum
     * @param idCardName
     * @param password
     * @return
     */
    @FormUrlEncoded
    @POST("/Manage/Login")
    Call<Employee> login(@Field("JobNum") String jobNum, @Field("IdCardNum") String idCardName, @Field("PassWord") String password);

    /**
     * Work!
     * Show employee info by id
     *
     * @param id
     * @return
     */
    @GET("/Manage/GetEmployeeInfo")
    Call<Employee> showEmployeeInfoById(@Query("id") String id);


    /**
     * Edit working state of employee
     *
     * @param id
     * @param state
     * @return
     */
    @FormUrlEncoded
    @POST("/Manage/EditWorkingState")
    Call<ErrorResponse> editWorkingState(@Field("id") String id, @Field("workingstate") String state);

    /**
     * Release Berth
     * @param carparkingid
     * @param employeeid
     * @return
     */
    @FormUrlEncoded
    @POST("/Manage/UpdateCarParkingInfoForM")
    Call<RepayObject> confirmBerthRelease(@Field("carparkingid") String carparkingid, @Field("employeeid") String employeeid);


    // ------------------------- Car Section -----------------------//

    @FormUrlEncoded
    @POST("/Manage/AddCarParkingInfo")
    Call<Berth> addCarParkingInfo(
            @Field("parkId") String parkId,
            @Field("plateNumber") String plateNumber,
            @Field("employeeid") String employeeid);


    /**
     * Show Car Parking List by id and state
     *
     * @param parkid
     * @param state  0,空车位，1，已停，2预约
     * @return
     * @see
     */
    @GET("/Manage/GetBerthInfoStateList")
    Call<List<Berth>> showCarParkingListByState(
            @Query("parkId") String parkid,
            @Query("state") String state,
            @Query("indexPage") int page);


    /**
     * @param parkid
     * @param state
     * @param page
     * @return
     * @see com.lesmtech.goparking.SpotOccupiedActivity
     */
    @GET("/Manage/GetCarParkingListP")
    Call<List<Berth>> getCarParkingListP(
            @Query("parkid") String parkid,
            @Query("state") String state,
            @Query("indexPage") int page);


    // ------------------------- Parking Section -----------------------//

    /**
     * @param id
     * @param state
     * @param indexPage
     * @return
     * @see com.lesmtech.goparking.SpotOccupiedActivity
     */
    @GET("/Manage/GetParkInfoListP")
    Call<List<Berth>> showParkingList(@Query("ParkId") String id, @Query("state") String state, @Query("indexPage") int indexPage);

    /**
     * To show panel info in the main screenF
     *
     * @param parkid
     * @return
     */
    @GET("/Manage/GetAppHomeNum")
    Call<PanelInfo> showPanelInfo(@Query("parkId") String parkid);

    /**
     * To adjust berth num
     *
     * @param carparkingid id
     * @param berthNum     adjust berth num to berthNum
     * @return
     */
    @FormUrlEncoded
    @POST("/Manage/AdjustBerthInfo")
    Call<SuccessResponse> adjustBerthInfo(@Field("carparkingid") String carparkingid, @Field("BerthNum") String berthNum);


    // ------------------------- Statistic Section -----------------------//
    @GET("/Manage/GetConsumptionCount")
    Call<StatisticsResponse> getTodayReport(@Query("parkid") int parkid);

    // http://101.200.192.56/Manage/GetConsumptionCountH?StartTime=2015-10-24%2000:23:21.737&EndTime=2015-10-25%2005:23:21.737&ParkId=5
    // http://101.200.192.56/Manage/GetConsumptionCountH?StartTime=2015-10-24&EndTime=2015-10-25&ParkId=5
    @GET("/Manage/GetConsumptionCountH")
    Call<StatisticsResponse> getHisReport(@Query("startTime") String startTime, @Query("endTime") String endTime, @Query("parkid") String parkid);

    @GET("/Manage/GetConsumptionCountE")
    Call<SuccessResponse> getAmountOfCashOrder(@Query("employeeid") String employeeid, @Query("StartTime") String startTime, @Query("EndTime") String endTime);

    @GET("/Manage/GetCashChargesEmployee")
    Call<List<CashRecord>> getCashRecordList(@Query("employeeid") String employeeid, @Query("startTime") String startTime, @Query("endTime") String endTime, @Query("indexpage") int indexpage);


    // ------------------------- Order Section -----------------------//

    //    http://101.200.192.56:8081/Manage/GetBillStatistics
    @FormUrlEncoded
    @POST("Manage/GetBillStatistics")
    Call<Order> getOrderInfo(@Field("employeeid") String employeeid, @Field("CarParkingId") String carParkingId);

    /**
     * pay the order by cash.
     *
     * @param employeeid
     * @param orderinfoid
     * @return
     */
    @FormUrlEncoded
    @POST("/Manage/AddCashCharges")
    Call<SuccessResponse> finishOrderByCash(
            @Field("employeeid") String employeeid,
            @Field("orderinfoid") String orderinfoid,
            @Field("carparkingid") String carparkingid);


}
