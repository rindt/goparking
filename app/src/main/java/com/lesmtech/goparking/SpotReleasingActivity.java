package com.lesmtech.goparking;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;


import com.lesmtech.goparking.asyntask.GetCarParkingListByState;
import com.lesmtech.goparking.asyntask.SpotReleasingTask;
import com.lesmtech.goparking.entity.Berth;
import com.lesmtech.goparking.entity.Employee;
import com.lesmtech.goparking.entity.RepayObject;
import com.lesmtech.goparking.tool.DataFormatConvert;
import com.lesmtech.goparking.tool.GRetrofit;
import com.lesmtech.goparking.view.ConfirmLeftDialogView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * This is one of item on the panel activity
 * Show the cars that are going to release
 * <p/>
 * http://101.200.192.56:8081/Manage/GetCarParkingListP?ParkId=68&State=3&indexPage=1
 *
 * @author Rindt
 * @version 0.1
 * @since 10/26/15
 */
public class SpotReleasingActivity extends AppCompatActivity implements SpotReleasingTask.SpotReleasingTaskListener {

    // Working on the item in the list, might be removed
    private int selectedPosition;

    // 等待离场状态
    private String state = "3";

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.lv_car_releasing)
    ListView lvCarReleasing;

    @Bind(R.id.parent)
    ViewGroup parent;

    private List<Berth> berths;

    private boolean hasMoreData;

    private int page = 1;

    private String parkid;

    private String employeeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spotreleasing);
        ButterKnife.bind(this);

        berths = new ArrayList<>();

        // set toolbar
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.arrow_back);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));


        lvCarReleasing.setAdapter(mAdapter);

        SharedPreferences session = getSharedPreferences("session", MODE_PRIVATE);
        parkid = String.valueOf(session.getInt(Employee.PARKID, -1));
        employeeId = String.valueOf(session.getLong(Employee.ID, -1));

        if (!parkid.equals("-1")) {
            // request to get leaving cars
            requestForLeavingCar();
        }

    }

    private BaseAdapter mAdapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return berths.size();
        }

        @Override
        public Object getItem(int position) {
            return berths.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(SpotReleasingActivity.this).inflate(R.layout.list_item_spot_releasing, parent, false);
            }

            final Berth berth = berths.get(position);

            TextView plateNumber = (TextView) convertView.findViewById(R.id.plate_number);
            TextView time = (TextView) convertView.findViewById(R.id.time);
            TextView consupmtion = (TextView) convertView.findViewById(R.id.consumption);
            TextView berthNumber = (TextView) convertView.findViewById(R.id.berth_number);
            TextView timeSpend = (TextView) convertView.findViewById(R.id.spend_time);

            plateNumber.setText(berth.getPlateNumber());
            time.setText(DataFormatConvert.uglyDataToDuration(berth.getStartTime(), berth.getEndTime()));

            // Test
            consupmtion.setText(String.valueOf(berth.getConsumption()));
            berthNumber.setText(String.valueOf(berth.getBerthNum()));
            timeSpend.setText(DataFormatConvert.uglyDataToDuration(berth.getProcessTime()));

            System.out.println("Berth Num:" + berth.getBerthNum() + "; Berth Id:" + berth.getId());

            convertView.findViewById(R.id.action_leave).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPosition = position;
                    // Request the server to request leaving.
                    // Success : delete the record,
                    // Failure : Overtime... start dialog
                    SpotReleasingTask task = new SpotReleasingTask(String.valueOf(berth.getId()), employeeId);
                    task.setmSpotReleasingTaskListener(SpotReleasingActivity.this);
                    task.execute();
                }
            });

            if (hasMoreData && berths.size() - position < 5) {
                requestForLeavingCar();
            }
            return convertView;
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void openOverTimeDialog(final RepayObject object) {

        final AlertDialog mAlertDialog;

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);

        View view = LayoutInflater.from(this).inflate(R.layout.dialog_list_car_overtime, parent, false);

        mAlertDialog = mBuilder.setView(view).create();

        ((TextView) (view.findViewById(R.id.plate_number))).setText(object.getPlateNumber());

        (view.findViewById(R.id.action_pay)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("app", "Pay has been clicked!");
                // might use startForResult.... so the dialog can be dismiss.
                Intent intent = new Intent(SpotReleasingActivity.this, BillDialogActivity.class);
                intent.putExtra(BillDialogActivity.BILL_TYPE, BillDialogActivity.BILL_TYPE_OVERTIME);
                intent.putExtra(BillDialogActivity.RECORD_ID, object.getRecordId());
                intent.putExtra(BillDialogActivity.EMPLOYEE_ID, employeeId);
                startActivity(intent);
                mAlertDialog.dismiss();
            }
        });
        mAlertDialog.show();
    }

    private void requestForLeavingCar() {
        GetCarParkingListByState task = new GetCarParkingListByState();
        task.setmGetCarParkingListByStateListener(new GetCarParkingListByState.GetCarParkingListByStateListener() {
            @Override
            public void success(List<Berth> berths, boolean hasMoreData) {
                SpotReleasingActivity.this.hasMoreData = hasMoreData;
                SpotReleasingActivity.this.berths.addAll(berths);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure() {
                SpotReleasingActivity.this.hasMoreData = false;
            }
        });
        task.execute(parkid, state, String.valueOf(page++));
    }


    @Override
    public void successReleasing() {
        berths.remove(selectedPosition);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void needPayBaby(RepayObject object) {
        openOverTimeDialog(object);
    }

    @Override
    public void hasReleased() {

    }
}
