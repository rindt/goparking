package com.lesmtech.goparking;

import android.content.SharedPreferences;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.lesmtech.goparking.entity.Employee;
import com.lesmtech.goparking.entity.json.ErrorResponse;
import com.lesmtech.goparking.entity.json.SuccessResponse;
import com.lesmtech.goparking.tool.GRetrofit;

import java.io.IOException;
import java.util.IllegalFormatException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTouch;

/**
 * @author Rindt
 * @version 0.1
 * @since 10/24/15
 */
public class ChangePasswordActivity extends AppCompatActivity {

    private final static String TAG = "ChangePasswordActivity";

    private long id;

    private int EDIT_STATU_DEFAULE = 1;

    private int EDIT_STATU_ERROR = 2;

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.old_password)
    EditText oldPassword;

    @Bind(R.id.new_password)
    EditText newPassword;

    @Bind(R.id.re_new_password)
    EditText reNewPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        mToolbar.setTitle(getResources().getString(R.string.title_car_exit));
        mToolbar.setNavigationIcon(R.drawable.arrow_back);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        SharedPreferences session = getSharedPreferences("session", MODE_PRIVATE);
        id = session.getLong(Employee.ID, -1);

        refreshLevelBackground();
    }

    private String validateInput() {

        boolean isPassed = true;

        // Validate not null
        String opValue = oldPassword.getText().toString();

        String npValue = newPassword.getText().toString();

        String rnpValue = reNewPassword.getText().toString();

        if (opValue.equals("")) {
            refreshViewBackground(oldPassword, EDIT_STATU_ERROR);
            displayErrorMessage(getResources().getString(R.string.error_op_null));
            isPassed = false;
        }

        if (npValue.equals("")) {
            refreshViewBackground(newPassword, EDIT_STATU_ERROR);
            displayErrorMessage(getResources().getString(R.string.error_np_null));
            isPassed = false;
        }

        if (rnpValue.equals("")) {
            refreshViewBackground(reNewPassword, EDIT_STATU_ERROR);
            displayErrorMessage(getResources().getString(R.string.error_rnp_null));
            isPassed = false;
        }

        if (!rnpValue.equals(npValue)) {
            refreshViewBackground(reNewPassword, EDIT_STATU_ERROR);
            refreshViewBackground(newPassword, EDIT_STATU_ERROR);
            displayErrorMessage(getResources().getString(R.string.error_np_rnp_not_equal));
            isPassed = false;
        }

        if (isPassed) {
            return npValue;
        }
        return null;
    }

    @OnTouch({R.id.new_password, R.id.old_password, R.id.re_new_password})
    public boolean refreshWhenTouch(View view) {
        refreshViewBackground(view, EDIT_STATU_DEFAULE);
        return false;
    }

    private void refreshLevelBackground() {
        oldPassword.getBackground().setLevel(EDIT_STATU_DEFAULE);
        newPassword.getBackground().setLevel(EDIT_STATU_DEFAULE);
        reNewPassword.getBackground().setLevel(EDIT_STATU_DEFAULE);
    }

    private void refreshViewBackground(View view, int state) {
        view.getBackground().setLevel(state);
    }

    private void displayErrorMessage(String msg) {
        Toast.makeText(ChangePasswordActivity.this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.action_submit)
    public void actionSubmit() {

        String password;

        refreshLevelBackground();

        if ((password = validateInput()) != null) {
            // If pass the validate, request to server
            new ChangePasswordTask().execute(String.valueOf(id), password);
        }
    }

    public class ChangePasswordTask extends AsyncTask<String, Void, SuccessResponse> {
        @Override
        protected SuccessResponse doInBackground(String... params) {

            String id = params[0];

            String password = params[1];

            try {
                SuccessResponse response = GRetrofit.getInstance().changePassword(id, password).execute().body();
                if (response != null && response.getRowsCount() == 1) {
                    return response;
                }
            } catch (IllegalFormatException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(SuccessResponse successResponse) {
            if (successResponse != null) {
                displayErrorMessage(successResponse.getMsg());
                finish();
            }
            else{
                displayErrorMessage(getResources().getString(R.string.error_op_wrong));
            }
        }
    }

}
