package com.lesmtech.goparking;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.MutableBoolean;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.andexert.expandablelayout.library.ExpandableLayout;
import com.lesmtech.goparking.asyntask.GetCarResultByStateTask;
import com.lesmtech.goparking.entity.Berth;
import com.lesmtech.goparking.entity.Employee;
import com.lesmtech.goparking.entity.PanelInfo;
import com.lesmtech.goparking.tool.GRetrofit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author Rindt
 * @version 0.1
 * @since 10/25/15
 */
public class SpotLeftActivity extends AppCompatActivity {

    public final String STATE_OCCUPIED = "1";

    public final String STATE_AVAILABLE = "0";

    public final String STATE_RESERVED = "2";

    public boolean isInitial = true;

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    // The total number of spot
    @Bind(R.id.number_total)
    TextView numberTotal;

    @Bind(R.id.expandable_item_spot_left)
    ExpandableLayout itemSpotLeft;

    @Bind(R.id.expandable_item_spot_occupited)
    ExpandableLayout itemSpotOccupited;

    @Bind(R.id.expandable_item_spot_reserved)
    ExpandableLayout itemSpotReserved;

    private int theViewShowing = 0;

    public final int SPOT_LEFT = 0;

    public final int SPOT_OCCUPITED = 1;

    public final int SPOT_RESERVED = 2;


    public int CURRENT_STATE = 0;

    private ArrayList<View> headerContainers;

    // Row 1, 剩余车位
    ListView lvSpotLeft;

    // Row 2, 剩余车位
    ListView lvSpotOccupited;

    // Row 1, 剩余车位
    ListView lvSpotReserved;

    // Request Data
    private int parkId;

    private int page = 1;

    private boolean hasMoreData = true;

    ArrayList<Berth> mBerthsLeft;

    ArrayList<Berth> mBerthsReserved;

    ArrayList<Berth> mBerthsOccupited;

    private BerthsAdapter mBerthsLeftAdapter;

    private BerthsAdapter mBerthsReservedAdapter;

    private BerthsAdapter mBerthsOccpiedAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spotleft);
        ButterKnife.bind(this);

        mBerthsReserved = new ArrayList<>();
        mBerthsLeft = new ArrayList<>();
        mBerthsOccupited = new ArrayList<>();
        mBerthsLeftAdapter = new BerthsAdapter(mBerthsLeft);
        mBerthsReservedAdapter = new BerthsAdapter(mBerthsReserved);
        mBerthsOccpiedAdapter = new BerthsAdapter(mBerthsOccupited);

        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.arrow_back);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));

        SharedPreferences session = getSharedPreferences("session", MODE_PRIVATE);
        parkId = session.getInt(Employee.PARKID, -1);

        init();

        // Request server to get cars
        if (parkId != -1) {
            // Also request static of berths
            requestBerthsStatic(String.valueOf(parkId));
            // State 0 means the berth is occupied
            requestBerthsByState();
        }

    }

    private void init() {

        headerContainers = new ArrayList<>();

        itemSpotLeft.setTag(SPOT_LEFT);
        itemSpotOccupited.setTag(SPOT_OCCUPITED);
        itemSpotReserved.setTag(SPOT_RESERVED);

        headerContainers.add(itemSpotLeft.getHeaderLayout().findViewById(R.id.container));
        headerContainers.add(itemSpotOccupited.getHeaderLayout().findViewById(R.id.container));
        headerContainers.add(itemSpotReserved.getHeaderLayout().findViewById(R.id.container));

        lvSpotLeft = (ListView) itemSpotLeft.getContentLayout().findViewById(R.id.lv_content);
//        lvSpotLeft.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arrayLeft));

        lvSpotOccupited = (ListView) itemSpotOccupited.getContentLayout().findViewById(R.id.lv_content);
//        lvSpotOccupited.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arrayOccupied));

        lvSpotReserved = (ListView) itemSpotReserved.getContentLayout().findViewById(R.id.lv_content);
//        lvSpotReserved.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arrayReserved));

        headerContainers.get(SPOT_LEFT).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CURRENT_STATE = SPOT_LEFT;
                whichViewHasBeenClicked(itemSpotLeft);
            }
        });

        headerContainers.get(SPOT_OCCUPITED).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CURRENT_STATE = SPOT_OCCUPITED;
                whichViewHasBeenClicked(itemSpotOccupited);
            }
        });

        headerContainers.get(SPOT_RESERVED).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CURRENT_STATE = SPOT_RESERVED;
                whichViewHasBeenClicked(itemSpotReserved);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showExpandedView(ExpandableLayout view) {

        int tag = (int) view.getTag();
        view.show();
        headerContainers.get(tag).setBackgroundResource(R.color.lightBlue);
        // hide other
        for (int i = 0; i < 3; i++) {
            if ((int) view.getTag() != i) {
                headerContainers.get(i).setBackgroundResource(R.color.gray);
            }
        }
    }

    private void whichViewHasBeenClicked(ExpandableLayout v) {

        hasMoreData = true;
        page = 1;

        int tag = (int) v.getTag();

        if (theViewShowing == tag) {
            headerContainers.get(theViewShowing).setBackgroundResource(R.color.gray);
            v.hide();
            theViewShowing = -1;
            return;
        }

        switch (tag) {
            case SPOT_LEFT:
                mBerthsLeft.clear();
                requestBerthsByState();
                lvSpotLeft.setAdapter(mBerthsLeftAdapter);
                showExpandedView(itemSpotLeft);
                itemSpotOccupited.hide();
                itemSpotReserved.hide();
                break;
            case SPOT_RESERVED:
                mBerthsReserved.clear();
                requestBerthsByState();
                lvSpotReserved.setAdapter(mBerthsReservedAdapter);
                showExpandedView(itemSpotReserved);
                itemSpotLeft.hide();
                itemSpotOccupited.hide();
                break;
            case SPOT_OCCUPITED:
                mBerthsOccupited.clear();
                requestBerthsByState();
                lvSpotOccupited.setAdapter(mBerthsOccpiedAdapter);
                showExpandedView(itemSpotOccupited);
                itemSpotLeft.hide();
                itemSpotReserved.hide();
                break;
            default:
                break;
        }
        theViewShowing = tag;
    }


    public class GetCarResultFromServerByState extends AsyncTask<String, Void, List<Berth>> {

        int state;

        @Override
        protected List<Berth> doInBackground(String... params) {

            String parkid = params[0];
            int page = Integer.valueOf(params[1]);
            state = Integer.valueOf(params[2]);

            try {
                List<Berth> berths = GRetrofit.getInstance().getCarParkingListP(parkid, String.valueOf(state), page).execute().body();
                if (berths != null && berths.size() < 10) {
                    hasMoreData = false;
                }
                if (berths.size() != 0) {
                    return berths;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                Log.d("app", "No more data");
                hasMoreData = false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Berth> berths) {
            if (berths != null) {
                if(state == SPOT_LEFT){
                    mBerthsLeft.addAll(berths);
                    mBerthsLeftAdapter.notifyDataSetChanged();
                }else if(state == SPOT_RESERVED){
                    mBerthsReserved.addAll(berths);
                    mBerthsReservedAdapter.notifyDataSetChanged();
                }else if(state == SPOT_OCCUPITED){
                    mBerthsOccupited.addAll(berths);
                    mBerthsOccpiedAdapter.notifyDataSetChanged();
                }
                if (isInitial) {
                    lvSpotLeft.setAdapter(mBerthsLeftAdapter);
                    itemSpotLeft.show();
                    isInitial = false;
                }
            }
        }
    }

    private void requestBerthsByState() {
        new GetCarResultFromServerByState().execute(String.valueOf(parkId), String.valueOf(page++), String.valueOf(CURRENT_STATE));
    }

    public class BerthsAdapter extends BaseAdapter {

        public List<Berth> mBerthList;

        public BerthsAdapter(List<Berth> mBerthList){
            this.mBerthList = mBerthList;
        }

        @Override
        public int getCount() {
            return mBerthList.size();
        }

        @Override
        public Object getItem(int position) {
            return mBerthList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.list_item_spot_static, parent, false);
            }

            if (hasMoreData && position + 5 > mBerthList.size()) {
                // request to get more data.
                requestBerthsByState();
            }
            TextView number = (TextView) convertView.findViewById(R.id.number_spot);
            number.setText(String.valueOf(mBerthList.get(position).getBerthNum()));
            return convertView;
        }
    }

    private void requestBerthsStatic(String parkId) {
        new GetCarParkingInfo().execute(parkId);
    }

    public class GetCarParkingInfo extends AsyncTask<String, Void, PanelInfo> {

        @Override
        protected PanelInfo doInBackground(String... params) {

            String parkId = params[0];

            try {
                PanelInfo panelInfo = GRetrofit.getInstance().showPanelInfo(parkId).execute().body();
                if (panelInfo != null && panelInfo.getBerthcount() != 0) {
                    return panelInfo;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(PanelInfo panelInfo) {
            if (panelInfo != null) {
                ((TextView) headerContainers.get(SPOT_LEFT).findViewById(R.id.total_number)).setText(String.valueOf(panelInfo.getNumberOfLeft()));
                ((TextView) headerContainers.get(SPOT_RESERVED).findViewById(R.id.total_number)).setText(String.valueOf(panelInfo.getNumberOfReserved()));
                ((TextView) headerContainers.get(SPOT_OCCUPITED).findViewById(R.id.total_number)).setText(String.valueOf(panelInfo.getNumberOfOccupied() + panelInfo.getNumberOfLeaving()));
                numberTotal.setText(String.valueOf(panelInfo.getBerthcount()));
            }
        }
    }
}
