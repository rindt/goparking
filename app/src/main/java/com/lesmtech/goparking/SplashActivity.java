package com.lesmtech.goparking;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.lesmtech.goparking.entity.Employee;
import com.lesmtech.goparking.fragment.boarding.SignInFragment;

import cn.jpush.android.api.JPushInterface;

/**
 * @author Rindt
 * @version 0.1
 * @since 9/29/15
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (checkIfManagerHasLogIn()) {
            // Start Home Activity
            Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
            startActivity(intent);
            finish();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new SignInFragment()).commit();
        }
    }

    private boolean checkIfManagerHasLogIn() {
        SharedPreferences session = getSharedPreferences("session", MODE_PRIVATE);
        return session.getLong(Employee.ID, -1) != -1;
    }

    @Override
    protected void onResume() {
        super.onResume();
        JPushInterface.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        JPushInterface.onPause(this);
    }
}
