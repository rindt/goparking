package com.lesmtech.goparking;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.lesmtech.goparking.entity.Employee;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Rindt
 * @version 0.1
 * @since 9/29/15
 */
public class EditProfileActivity extends AppCompatActivity {

    @Bind(R.id.fullName)
    EditText mFullName;

    @Bind(R.id.telPhone)
    EditText mTelPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);
        ButterKnife.bind(this);
        renderViewWithSharedPreference();
    }

    private void renderViewWithSharedPreference() {
        SharedPreferences session = getSharedPreferences("session", MODE_PRIVATE);
        mFullName.setText(session.getString(Employee.FULLNAME, null));
        mTelPhone.setText(session.getString(Employee.TELPHONE, null));
        //....F
    }

    @OnClick(R.id.action_submit)
    public void actionSubmit() {
        new EditEmployeeProfile().execute(mFullName.getText().toString(), mTelPhone.getText().toString());
    }

    public class EditEmployeeProfile extends AsyncTask<String, Void, Employee> {
        @Override
        protected Employee doInBackground(String... params) {
//            Employee employee = GRetrofit.getInstance().editEmployeeInfo(params[0],params[1
            return null;
        }

        @Override
        protected void onPostExecute(Employee employee) {
            if (employee != null) {
                // Update SharedPreference
            }
            return;
        }
    }

}
