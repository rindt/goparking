package com.lesmtech.goparking.tool;

/**
 * @author Rindt
 * @version 0.1
 * @since 12/15/15
 */
public class Utils {

    public static String formatBerthNumberDigits(String number) {

        char[] numbers = number.toCharArray();
        for(char digit : numbers){
            if(!Character.isDigit(digit)){
                return null;
            }
        }
        // So the number is a number
        switch (number.length()){
            case 1:
                return "00" + number;
            case 2:
                return "0" + number;
            case 3:
                return number;
            default:
                return null;
        }
    }
}
