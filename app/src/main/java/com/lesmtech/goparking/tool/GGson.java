package com.lesmtech.goparking.tool;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author Rindt
 * @version 0.1
 * @since 9/27/15
 */
public final class GGson {

    private static Gson singleton;

    private GGson(){

    }

    public static Gson getInstance(){
        if(singleton ==  null){
            final GsonBuilder mBuilder = new GsonBuilder();
            singleton = mBuilder.create();
        }
        return singleton;
    }

}
