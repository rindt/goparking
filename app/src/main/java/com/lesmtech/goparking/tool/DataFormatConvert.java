package com.lesmtech.goparking.tool;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Rindt
 * @version 0.1
 * @since 10/17/15
 */
public class DataFormatConvert {

    /**
     * Return date in specified format.
     *
     * @param milliSeconds Date in milliseconds
     * @param dateFormat   Date format
     * @return String representing date in specified format
     */
    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        String dateString = formatter.format(new Date(milliSeconds));
        return dateString;
    }

    public static String uglyDataToDuration(String startTime, String endTime){
        String[] start = startTime.split("\\(");
        String[] end = endTime.split("\\(");
        String sTime = start[1].substring(0, start[1].length() - 2);
        String eTime = end[1].substring(0, end[1].length() - 2);
        long duration = Long.parseLong(eTime) - Long.parseLong(sTime);
        return getDate(duration, "hh:mm:ss");
    }

    public static String uglyDataToDuration(String time){
        String[] start = time.split("\\(");
        String sTime = start[1].substring(0, start[1].length() - 2);
        return getDate(Long.parseLong(sTime), "hh:mm:ss");
    }

    // Parse "/Date(1444444444444444)/"
    public static String uglyDataToMill(String uglyData){
        String shrinkUgly = uglyData.substring(6, uglyData.length() - 2);
        return getDate(Long.parseLong(shrinkUgly), "MM.dd hh:mm");
    }

}