package com.lesmtech.goparking.tool;

import com.lesmtech.goparking.service.GoParkingService;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * @author Rindt
 * @version 0.1
 * @since 9/27/15
 */
public class GRetrofit {

    public static String BASEURL = "http://101.200.192.56:8082";

    private static GoParkingService singleton;

    private GRetrofit(){

    }

    public static GoParkingService getInstance(){
        if(singleton == null){
            singleton = new Retrofit.Builder().baseUrl(BASEURL).addConverterFactory(GsonConverterFactory.create()).build().create(GoParkingService.class);
        }
        return singleton;
    }

}
