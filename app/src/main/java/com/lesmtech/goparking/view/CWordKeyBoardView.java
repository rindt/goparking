package com.lesmtech.goparking.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.lesmtech.goparking.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * This view is used to show number key board
 *
 * @author Rindt
 * @version 0.1
 * @since 10/6/15
 */
public class CWordKeyBoardView extends FrameLayout {

    private String selectedCharacter = "";

    private View theViewFocusing;

    @Bind(R.id.arrow_up)
    ImageView arrow;

    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void theSelectedItem(String content);
    }

    @Bind({R.id.a, R.id.b, R.id.c, R.id.d, R.id.e, R.id.f, R.id.g, R.id.h,
            R.id.i, R.id.j, R.id.k, R.id.l, R.id.m, R.id.n, R.id.o, R.id.p,
            R.id.q, R.id.r, R.id.s, R.id.t, R.id.u, R.id.v, R.id.w, R.id.x, R.id.y, R.id.z, R.id.z1, R.id.z2, R.id.z3})
    List<TextView> ewords;

    public CWordKeyBoardView(Context context) {
        super(context);
        initView(context);
    }

    public CWordKeyBoardView(Context context, String defaultCharacter) {
        super(context);
        selectedCharacter = defaultCharacter;
        initView(context);
    }

    public CWordKeyBoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public CWordKeyBoardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_cwordkeyboard, null);
        addView(view);
        ButterKnife.bind(this);

        String[] placeLocationR = getResources().getStringArray(R.array.places);
        for (int i = 0; i < ewords.size(); i++) {
            ewords.get(i).setText(placeLocationR[i]);
        }

        setSelectedCharacter(selectedCharacter);
    }

    public void setSelectedCharacter(String selectedCharacter) {
        for (TextView item : ewords) {
            if (selectedCharacter.equals(item.getText().toString())) {
                item.setTextColor(getResources().getColor(R.color.lightBlue));
            }
        }
    }

    /**
     * The key function to move and show keyboard with an arrow.
     *
     * @param view           the arrow pin to the point based on the view
     * @param selectedCharacter the number needs to be selected in the keyboard
     */
    public void pinTheArrowToTheView(View view, String selectedCharacter) {
        // Click again the view, hide the panel;
        if (theViewFocusing == view) {
            hideMySelf();
            return;
        } else {
            theViewFocusing = view;
            showMySelf();
        }

        setSelectedCharacter(selectedCharacter);

        Log.d("x_view:", " " + getRelativeLeft(view));
        Log.d("y_view:", " " + getRelativeTop(view));

        // Bug. the position doesn't change
        Log.d("x_arrow:", " " + getRelativeLeft(arrow));
        Log.d("y_arrow:", " " + getRelativeTop(arrow));

        float next_arrow_x = (getRelativeLeft(view) + view.getWidth() / 2 - arrow.getWidth() / 2);

        Log.d("next_arrow_x:", " " + next_arrow_x);
//      Log.d("next_arrow_y:", " " + (getRelativeTop(view) - arrow.getHeight()));

        float diff = Math.abs(getRelativeLeft(arrow) - next_arrow_x);

        Log.d("diff", " " + diff);

        if (next_arrow_x - getRelativeLeft(arrow) > 0) {
            moveArrowToPosition(arrow.getX() + diff, arrow.getY());
        } else {
            moveArrowToPosition(arrow.getX() - diff, arrow.getY());
        }
    }

    // Move the arrow to the right position, based on view position map
    private void moveArrowToPosition(float x, float y) {
        arrow.setX(x);
        arrow.setY(y);
    }

    // Set Listener
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private float getRelativeLeft(View myView) {
        if (myView.getParent() == myView.getRootView())
            return myView.getX();
        else
            return myView.getX() + getRelativeLeft((View) myView.getParent());
    }

    private float getRelativeTop(View myView) {
        if (myView.getParent() == myView.getRootView())
            return myView.getY();
        else
            return myView.getY() + getRelativeTop((View) myView.getParent());
    }

    public void hideMySelf() {
        // Click third time, it still shows up
        theViewFocusing = null;
        refreshItemToDefaultState();
        this.setVisibility(GONE);
    }

    public void showMySelf() {
        refreshItemToDefaultState();
        this.setVisibility(VISIBLE);
    }

    private void refreshItemToDefaultState() {
        for (TextView item : ewords) {
            item.setTextColor(getResources().getColor(R.color.black));
        }
    }

    @OnClick({R.id.a, R.id.b, R.id.c, R.id.d, R.id.e, R.id.f, R.id.g, R.id.h,
            R.id.i, R.id.j, R.id.k, R.id.l, R.id.m, R.id.n, R.id.o, R.id.p,
            R.id.q, R.id.r, R.id.s, R.id.t, R.id.u, R.id.v, R.id.w, R.id.x, R.id.y, R.id.z, R.id.z1, R.id.z2, R.id.z3})
    void actionSelectedCharactor(View view) {
        ((TextView) view).setTextColor(getResources().getColor(R.color.lightBlue));
        onItemClickListener.theSelectedItem(((TextView) view).getText().toString());
        hideMySelf();
    }
}
