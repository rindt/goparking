package com.lesmtech.goparking.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.admin.SystemUpdatePolicy;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.lesmtech.goparking.BillDialogActivity;
import com.lesmtech.goparking.CarExitActivity;
import com.lesmtech.goparking.R;
import com.lesmtech.goparking.entity.Berth;
import com.lesmtech.goparking.entity.ParkUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Rindt
 * @version 0.1
 * @since 10/4/15
 */
public class ConfirmLeftDialogView extends FrameLayout {

    private Context mContext;

    private Berth mBerth;

    private AlertDialog dialog;

    public DialogViewResultListener mDialogViewResultListener;

    public interface DialogViewResultListener{
        void success(int orderId);
        void fail();
    }

    @Bind(R.id.title)
    TextView title;

    public ConfirmLeftDialogView(Context context) {
        super(context);
    }

    public ConfirmLeftDialogView(Context context, Berth berth) {
        super(context);
        mContext = context;
        mBerth = berth;
        initView();
    }

    public ConfirmLeftDialogView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public ConfirmLeftDialogView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public ConfirmLeftDialogView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.view_confirm_leave, null);
        addView(view);
        ButterKnife.bind(this);
        title.setText(mBerth.getPlateNumber());
    }

    @OnClick(R.id.action_cancel)
    void actionCancel() {
        fadeOut();
        mDialogViewResultListener.fail();
    }

    @OnClick(R.id.action_submit)
    void actionSubmit() {
        fadeOut();
        // Start Activity in MainActivity, parse parking record id
        mDialogViewResultListener.success(mBerth.getId());
    }

    public void setDialog(AlertDialog dialog) {
        this.dialog = dialog;
    }

    private void fadeOut() {
        Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        this.startAnimation(anim);
        dialog.dismiss();
    }

    public void setmDialogViewResultListener(DialogViewResultListener mDialogViewResultListener) {
        this.mDialogViewResultListener = mDialogViewResultListener;
    }
}
