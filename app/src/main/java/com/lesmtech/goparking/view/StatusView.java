package com.lesmtech.goparking.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.lesmtech.goparking.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author Rindt
 * @version 0.1
 * @since 10/1/15
 */
public class StatusView extends FrameLayout {

    @Bind(R.id.icon)
    ImageView icon;

    @Bind(R.id.state)
    TextView state;

    private int[] checkIcons = new int[]{R.drawable.message_checkmark_gray, R.drawable.message_checkmark_red, R.drawable.message_cross_gray, R.drawable.message_cross_red};

    private int[] states = new int[]{
            R.string.label_paid, R.string.label_unpaid
    };

    private boolean checked = false;

    private boolean paid = false;

    public StatusView(Context context) {
        super(context);
        initView(context);
    }

    public StatusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray mTypedArray = context.obtainStyledAttributes(attrs, R.styleable.StatusView, 0, 0);
        try {
            checked = mTypedArray.getBoolean(R.styleable.StatusView_states, false);
            paid = mTypedArray.getBoolean(R.styleable.StatusView_paid, false);
        } finally {
            mTypedArray.recycle();
        }
        initView(context);
    }

    public StatusView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public StatusView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }

    private void initView(Context context) {
        View child = LayoutInflater.from(context).inflate(R.layout.view_status, null);
        addView(child);

        ButterKnife.bind(this);

        setChecked(checked);
    }

    public void setChecked(boolean check) {
        if (paid) {
            if (check) {
                icon.setBackground(getResources().getDrawable(checkIcons[1]));
            } else {
                icon.setBackground(getResources().getDrawable(checkIcons[0]));
            }
            state.setText(states[0]);
        } else {
            if (check) {
                icon.setBackground(getResources().getDrawable(checkIcons[3]));
            } else {
                icon.setBackground(getResources().getDrawable(checkIcons[2]));
            }
            state.setText(states[1]);
        }
    }

}
