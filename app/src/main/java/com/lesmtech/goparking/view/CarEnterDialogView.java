package com.lesmtech.goparking.view;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.lesmtech.goparking.R;
import com.lesmtech.goparking.entity.Berth;
import com.lesmtech.goparking.entity.json.SuccessResponse;
import com.lesmtech.goparking.tool.GRetrofit;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Rindt
 * @version 0.1
 * @see com.lesmtech.goparking.CarEnterActivity
 * @since 10/26/15
 */
public class CarEnterDialogView extends FrameLayout {

    @Bind(R.id.plate_number)
    TextView plateNumber;

    @Bind(R.id.berth_number)
    TextView numberOfBerth;

    private Context mContext;

    private AlertDialog dialog;

    private Berth mBerth;

    public CarEnterDialogListener mCarEnterDialogListener;

    public void setmCarEnterDialogListener(CarEnterDialogListener mCarEnterDialogListener) {
        this.mCarEnterDialogListener = mCarEnterDialogListener;
    }

    public interface CarEnterDialogListener {
        void edit(Berth berth);
        void cancel();
    }

    public CarEnterDialogView(Context context, Berth mBerth) {
        super(context);
        mContext = context;
        this.mBerth = mBerth;
        initView();
    }

    public CarEnterDialogView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public CarEnterDialogView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public CarEnterDialogView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_car_enter_confirm, null);
        addView(view);
        ButterKnife.bind(this);
        plateNumber.setText(mBerth.getPlateNumber());
        numberOfBerth.setText(String.valueOf(mBerth.getBerthNum()));
    }

    public void setDialog(AlertDialog dialog) {
        this.dialog = dialog;
    }

    private void fadeOut() {
        Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        this.startAnimation(anim);
        dialog.dismiss();
    }

    @OnClick(R.id.action_edit)
    void actionCancel(){
        mCarEnterDialogListener.edit(mBerth);
        fadeOut();
    }

    @OnClick(R.id.action_submit)
    void actionSubmit(){
        // Confirm to adjust Berth Num
        mCarEnterDialogListener.cancel();
        fadeOut();
    }
}
