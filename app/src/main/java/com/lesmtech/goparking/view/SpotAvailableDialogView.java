package com.lesmtech.goparking.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.lesmtech.goparking.R;
import com.lesmtech.goparking.entity.Berth;
import com.lesmtech.goparking.entity.Employee;
import com.lesmtech.goparking.tool.GRetrofit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * The data requests earlier in activity just for performance.
 * This is used to select an available spot among all. Dialog
 * <p/>
 * Listener should be used in the activity
 * 1) success (1)
 * 2) fail()
 *
 * @author Rindt
 * @version 0.1
 * @see com.lesmtech.goparking.CarEnterActivity
 * @since 10/26/15
 */
public class SpotAvailableDialogView extends FrameLayout implements SpotAdjustConfirmDialogView.SpotAdjustConfirmDialogListener {

    private Context mContext;

    private AlertDialog dialog;

    private ArrayList<Berth> mBerths;

    private Berth mBerth;

    private String parkId;

    private int indexPage = 1;

    private String id;

    public DialogViewResultListener mDialogViewResultListener;

    @Override
    public void success(String selectedSpotNumber) {
        mDialogViewResultListener.success(selectedSpotNumber);
        fadeOut();
    }

    @Override
    public void fail() {
        mDialogViewResultListener.fail();
        fadeOut();
    }

    public interface DialogViewResultListener {

        void success(String selectedSpotNumber);

        void fail();
    }

    @Bind(R.id.lv_left_spot)
    ListView lvSpotAvaileble;

    public SpotAvailableDialogView(Context context, Berth berth) {
        super(context);
        mContext = context;
        mBerth = berth;
        initView();
    }

    public SpotAvailableDialogView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public SpotAvailableDialogView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public SpotAvailableDialogView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_list_spot_available, null);
        addView(view);
        ButterKnife.bind(this);
        mBerths = new ArrayList<>();

        SharedPreferences session = mContext.getSharedPreferences("session", Context.MODE_PRIVATE);
        parkId = String.valueOf(session.getInt(Employee.PARKID, -1));
        id = String.valueOf(session.getLong(Employee.ID, -1));

        lvSpotAvaileble.setAdapter(mSpotsAdapter);
        lvSpotAvaileble.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Task to update the berth number .....
                Berth berth = mBerths.get(position);
                showConfirmDialog(mBerth, String.valueOf(berth.getBerthNum()));
//                mDialogViewResultListener.success(String.valueOf(mBerths.get(position).getBerthNum()));
//                fadeOut();
            }
        });

        new SpotAvailableTask().execute(parkId, "0", String.valueOf(indexPage));
    }

    public void setDialog(AlertDialog dialog) {
        this.dialog = dialog;
    }

    private void fadeOut() {
        Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        this.startAnimation(anim);
        dialog.dismiss();
    }

    public void setmDialogViewResultListener(DialogViewResultListener mDialogViewResultListener) {
        this.mDialogViewResultListener = mDialogViewResultListener;
    }

    private BaseAdapter mSpotsAdapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return mBerths.size();
        }

        @Override
        public Object getItem(int position) {
            return mBerths.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.list_item_spot_available, parent, false);
            }
            TextView number = (TextView) (convertView.findViewById(R.id.number_spot));
            number.setText(String.valueOf(mBerths.get(position).getBerthNum()));
            return convertView;
        }
    };

    public class SpotAvailableTask extends AsyncTask<String, Void, List<Berth>> {

        @Override
        protected List<Berth> doInBackground(String... params) {

            String parkid = params[0];

            String id = params[1];

            String index = params[2];

            try {
                List<Berth> berths = GRetrofit.getInstance().showCarParkingListByState(parkid, id, Integer.valueOf(index)).execute().body();
                if (berths != null && berths.size() != 0) {
                    return berths;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Berth> berths) {
            if (berths != null) {
                mBerths.addAll(berths);
                mSpotsAdapter.notifyDataSetChanged();
            }
        }
    }

    private void showConfirmDialog(Berth berth, String selectedSpotNumber) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
        SpotAdjustConfirmDialogView view = new SpotAdjustConfirmDialogView(mContext, berth, selectedSpotNumber);
        view.setmSpotAdjustConfirmDialogListener(this);
        final AlertDialog mAlertDialog;
        mAlertDialog = mBuilder.setView(view).create();
        view.setDialog(mAlertDialog);
        WindowManager.LayoutParams wmlp = mAlertDialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER;
        mAlertDialog.show();
    }
}
