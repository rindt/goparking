package com.lesmtech.goparking.view;

import android.app.AlertDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.lesmtech.goparking.R;
import com.lesmtech.goparking.SpotOccupiedActivity;
import com.lesmtech.goparking.entity.Berth;
import com.lesmtech.goparking.entity.Car;
import com.lesmtech.goparking.tool.DataFormatConvert;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Rindt
 * @version 0.1
 * @see com.lesmtech.goparking.SpotOccupiedActivity
 * @since 10/5/15
 */
public class SpotOccupiedCarView extends FrameLayout implements SpotAvailableDialogView.DialogViewResultListener {

    private Berth mBerth;

    private Context mContext;

    @Bind(R.id.plateNumber)
    TextView mPlateNumber;

    @Bind(R.id.time_of_parking)
    TextView mTimeDuration;

    @Bind(R.id.number_of_park_unit)
    TextView numberOfParkUnit;

    public SpotOccupiedCarView(Context context, Berth berth) {
        super(context);
        mBerth = berth;
        mContext = context;
        initView();
    }

    public SpotOccupiedCarView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpotOccupiedCarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SpotOccupiedCarView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item_spot_occupied_car, null);
        addView(view);
        ButterKnife.bind(this);
        if (mBerth != null) {
            setBerth(mBerth);
        }
    }

    @OnClick(R.id.action_edit)
    void actionEdit() {
        Log.d("app", "action_edit");
        // Show dialog
        showSpotAvailableDialog();
    }

    private void showSpotAvailableDialog() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
        SpotAvailableDialogView view = new SpotAvailableDialogView(mContext, mBerth);
        view.setmDialogViewResultListener(this);
        final AlertDialog mAlertDialog;
        mAlertDialog = mBuilder.setView(view).create();
        view.setDialog(mAlertDialog);
        WindowManager.LayoutParams wmlp = mAlertDialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER;
        mAlertDialog.show();
    }

    @Override
    public void success(String selectedSpotNumber) {
        // after selected
        numberOfParkUnit.setText(selectedSpotNumber);
    }

    @Override
    public void fail() {
    }

    // Should fix this part . The api doesn't support
    public void setBerth(Berth berth) {
        // recreate car object and set values
        mPlateNumber.setText(berth.getPlateNumber());

        // The api current doesn't has this two value...
        if (berth.getStartTime() != null || berth.getEndTime() != null) {
            String duration = DataFormatConvert.uglyDataToDuration(berth.getStartTime(), berth.getEndTime());
            mTimeDuration.setText(duration);
        }
        numberOfParkUnit.setText(String.valueOf(berth.getBerthNum()));
    }
}
