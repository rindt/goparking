package com.lesmtech.goparking.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.lesmtech.goparking.R;

import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * This view is used to show number key board
 *
 * @author Rindt
 * @version 0.1
 * @since 10/6/15
 */
public class KeyBoardView extends FrameLayout {

    public final static int UP = 0;

    public final static int DOWN = 1;

    private int direction = 0; // default

    private int selectedNumber = 0;

    private View theViewFocusing;

    @Bind({R.id.zero, R.id.one, R.id.two, R.id.three, R.id.four, R.id.five, R.id.six, R.id.seven, R.id.eight, R.id.nine})
    List<TextView> numbersView;

    @Bind(R.id.arrow_down)
    ImageView arrow;

    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void theSelectedItem(String content);
    }

    public KeyBoardView(Context context) {
        super(context);
        initView(context);
    }

    public KeyBoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public KeyBoardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public KeyBoardView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }

    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_numberkeyboard, null);
        addView(view);
        ButterKnife.bind(this);
    }

    /**
     * The key function to move and show keyboard with an arrow.
     *
     * @param view           the arrow pin to the point based on the view
     * @param selectedNumber the number needs to be selected in the keyboard
     */
    public void pinTheArrowToTheView(View view, int selectedNumber) {
        // Click again the view, hide the panel;
        if (theViewFocusing == view) {
            hideMySelf();
            return;
        } else {
            theViewFocusing = view;
            showMySelf();
        }

        // if selected number == -1, do nothing, that means it is a charactor, then it has to be showed in the eword key board
        if(selectedNumber != -1) {
            numbersView.get(selectedNumber).setTextColor(getResources().getColor(R.color.lightBlue));
        }

        Log.d("x_view:", " " + getRelativeLeft(view));
        Log.d("y_view:", " " + getRelativeTop(view));

        // Bug. the position doesn't change
        Log.d("x_arrow:", " " + getRelativeLeft(arrow));
        Log.d("y_arrow:", " " + getRelativeTop(arrow));

        float next_arrow_x = (getRelativeLeft(view) + view.getWidth() / 2 - arrow.getWidth() / 2);

        Log.d("next_arrow_x:", " " + next_arrow_x);
//      Log.d("next_arrow_y:", " " + (getRelativeTop(view) - arrow.getHeight()));

        float diff = Math.abs(getRelativeLeft(arrow) - next_arrow_x);

        Log.d("diff", " " + diff);

        if (next_arrow_x - getRelativeLeft(arrow) > 0) {
            moveArrowToPosition(arrow.getX() + diff, arrow.getY());
        } else {
            moveArrowToPosition(arrow.getX() - diff, arrow.getY());
        }
    }

    /**
     * Hasn't implement
     * Set KeyBoard Direction
     *
     * @param direction
     */
    public void setKeyBoardDirection(int direction) {
        this.direction = direction;
    }

    // Move the arrow to the right position, based on view position map
    private void moveArrowToPosition(float x, float y) {
        arrow.setX(x);
        arrow.setY(y);
    }

    // Set Listener
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private float getRelativeLeft(View myView) {
        if (myView.getParent() == myView.getRootView())
            return myView.getX();
        else
            return myView.getX() + getRelativeLeft((View) myView.getParent());
    }

    private float getRelativeTop(View myView) {
        if (myView.getParent() == myView.getRootView())
            return myView.getY();
        else
            return myView.getY() + getRelativeTop((View) myView.getParent());
    }

    public void hideMySelf() {
        // Click third time, it still shows up
        theViewFocusing = null;
        refreshItemToDefaultState();
        this.setVisibility(GONE);
    }

    public void showMySelf() {
        refreshItemToDefaultState();
        this.setVisibility(VISIBLE);
    }

    /**
     * Convery the clicked String back
     *
     * @param view
     */
    @OnClick({R.id.zero, R.id.one, R.id.two, R.id.three, R.id.four, R.id.five, R.id.six, R.id.seven, R.id.eight, R.id.nine})
    void actionSelectNumber(View view) {
        onItemClickListener.theSelectedItem(((TextView) view).getText().toString());
        hideMySelf();
    }

    private void refreshItemToDefaultState() {
        for (TextView item : numbersView) {
            item.setTextColor(getResources().getColor(R.color.black));
        }
    }

}
