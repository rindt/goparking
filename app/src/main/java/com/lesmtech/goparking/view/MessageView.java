package com.lesmtech.goparking.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.lesmtech.goparking.R;
import com.lesmtech.goparking.entity.Message;

import butterknife.ButterKnife;

/**
 * @author Rindt
 * @version 0.1
 * @since 10/7/15
 */
public class MessageView extends FrameLayout {

    private Message message;

    private Context context;


    public MessageView(Context context) {
        super(context);
    }

    public MessageView(Context context, Message message) {
        super(context);
        this.message = message;
        this.context = context;
        initView();
    }

    public MessageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MessageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MessageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void initView() {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_message, null);
        addView(view);
        ButterKnife.bind(this);

        if (message != null) {
            setMessage(message);
        }

    }

    public void setMessage(Message message) {
        // Set message to view
    }

}
