package com.lesmtech.goparking.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.lesmtech.goparking.R;
import com.lesmtech.goparking.entity.Berth;
import com.lesmtech.goparking.tool.DataFormatConvert;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Rindt
 * @version 0.1
 * @see com.lesmtech.goparking.SpotReservedActivity
 * @since 10/5/15
 */
public class SpotReservedCarView extends FrameLayout {

    private Berth mBerth;

    private Context mContext;

    @Bind(R.id.plateNumber)
    TextView mPlateNumber;

    @Bind(R.id.time_of_parking)
    TextView mTimeDuration;

    @Bind(R.id.number_of_park_unit)
    TextView numberOfParkUnit;

    public SpotReservedCarView(Context context, Berth berth) {
        super(context);
        mBerth = berth;
        mContext = context;
        initView();
    }

    public SpotReservedCarView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpotReservedCarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SpotReservedCarView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item_spot_reserved_car, null);
        addView(view);
        ButterKnife.bind(this);
        if (mBerth != null) {
            setBerth(mBerth);
        }
    }

    public void setBerth(Berth berth) {

        System.out.println("Berth num:" + berth.getBerthNum() + "Berth id:" + berth.getId()) ;

        // recreate car object and set values
        mPlateNumber.setText(berth.getPlateNumber());
        // The api current doesn't has this two value...
        if(berth.getStartTime()!=null || berth.getEndTime() != null) {
            String duration = DataFormatConvert.uglyDataToDuration(berth.getStartTime(), berth.getEndTime());
            mTimeDuration.setText(duration);
        }
        numberOfParkUnit.setText(String.valueOf(berth.getId()));
    }
}
