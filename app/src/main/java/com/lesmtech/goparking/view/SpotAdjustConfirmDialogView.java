package com.lesmtech.goparking.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.IntegerRes;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.lesmtech.goparking.R;
import com.lesmtech.goparking.entity.Berth;
import com.lesmtech.goparking.entity.Employee;
import com.lesmtech.goparking.entity.json.SuccessResponse;
import com.lesmtech.goparking.tool.GRetrofit;
import com.lesmtech.goparking.tool.Utils;
import com.squareup.okhttp.internal.Util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * The data requests earlier in activity just for performance.
 * This is used to select an available spot among all. Dialog
 * <p/>
 * Listener should be used in the activity
 * 1) success (1)
 * 2) fail()
 *
 * @author Rindt
 * @version 0.1
 * @see com.lesmtech.goparking.CarEnterActivity
 * @since 10/26/15
 */
public class SpotAdjustConfirmDialogView extends FrameLayout {

    @Bind(R.id.plate_number)
    TextView plateNumber;

    @Bind(R.id.number_spot)
    TextView numberOfSpot;

    @Bind(R.id.adjust_info)
    TextView adjustInfo;

    private Context mContext;

    private AlertDialog dialog;

    private Berth mBerth;

    private String targetBerthNum;

    private String currentBerthNum;

    public SpotAdjustConfirmDialogListener mSpotAdjustConfirmDialogListener;

    public interface SpotAdjustConfirmDialogListener {

        void success(String selectedSpotNumber);

        void fail();
    }

    public SpotAdjustConfirmDialogView(Context context, Berth mBerth, String berthNum) {
        super(context);
        mContext = context;
        this.mBerth = mBerth;
        this.targetBerthNum = berthNum;
        initView();
    }

    public SpotAdjustConfirmDialogView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public SpotAdjustConfirmDialogView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public SpotAdjustConfirmDialogView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_list_spot_check_confirm, null);
        addView(view);
        ButterKnife.bind(this);
        plateNumber.setText(mBerth.getPlateNumber());
        numberOfSpot.setText(targetBerthNum);

        // Standard Format
        targetBerthNum = Utils.formatBerthNumberDigits(targetBerthNum);
        currentBerthNum = Utils.formatBerthNumberDigits(String.valueOf(mBerth.getBerthNum()));

        StringBuilder infoContent = new StringBuilder("泊位");
        infoContent.append(currentBerthNum).append("改为").append(targetBerthNum).append("?");

        SpannableString spannableString = new SpannableString(infoContent.toString());
        spannableString.setSpan(new ForegroundColorSpan(Color.RED), 2, 5, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(Color.RED), 7, 10, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        adjustInfo.setText(spannableString);
    }

    public void setDialog(AlertDialog dialog) {
        this.dialog = dialog;
    }

    private void fadeOut() {
        Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        this.startAnimation(anim);
        dialog.dismiss();
    }

    public void setmSpotAdjustConfirmDialogListener(SpotAdjustConfirmDialogListener mSpotAdjustConfirmDialogListener) {
        this.mSpotAdjustConfirmDialogListener = mSpotAdjustConfirmDialogListener;
    }

    public class SpotAdjustTask extends AsyncTask<String, Void, SuccessResponse> {

        @Override
        protected SuccessResponse doInBackground(String... params) {

            String carParkingId = params[0];
            String berthNum = params[1];

            try {
                SuccessResponse response = GRetrofit.getInstance().adjustBerthInfo(carParkingId, berthNum).execute().body();
                if (response != null && response.getRowsCount() == 1) {
                    return response;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(SuccessResponse response) {
            if (response != null) {
                // Parse back the changed berth Num to update view
                mSpotAdjustConfirmDialogListener.success(targetBerthNum);
                fadeOut();
            } else {
                mSpotAdjustConfirmDialogListener.fail();
                fadeOut();
            }
        }
    }

    @OnClick(R.id.action_cancel)
    void actionCancel() {
        mSpotAdjustConfirmDialogListener.fail();
        fadeOut();
    }

    @OnClick(R.id.action_submit)
    void actionSubmit() {
        // Confirm to adjust Berth Num
        new SpotAdjustTask().execute(String.valueOf(mBerth.getId()), targetBerthNum);
    }

}
