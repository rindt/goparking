package com.lesmtech.goparking.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.lesmtech.goparking.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author Rindt
 * @version 0.1
 * @since 9/29/15
 */
public class GridItemView extends FrameLayout {

    @Bind(R.id.number)
    TextView mNumber;

    @Bind(R.id.unit)
    TextView mUnit;

    @Bind(R.id.label)
    TextView label;

    public GridItemView(Context context) {
        super(context);
        initView(context);
    }

    public GridItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public GridItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public GridItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }

    private void initView(Context context) {
        View view = View.inflate(context, R.layout.view_grid_item, null);
        addView(view);
        ButterKnife.bind(this);
    }

    public void setData(int number, String unit, String content){
        mNumber.setText(String.valueOf(number));
        mUnit.setText(unit);
        label.setText(content);
    }

}
