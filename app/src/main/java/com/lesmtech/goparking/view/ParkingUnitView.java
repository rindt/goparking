package com.lesmtech.goparking.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.lesmtech.goparking.R;
import com.lesmtech.goparking.entity.Berth;
import com.lesmtech.goparking.entity.ParkUnit;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author Rindt
 * @version 0.1
 * @since 10/3/15
 */
public class ParkingUnitView extends FrameLayout {

    private Context mContext;

    private Berth mBerth;

    @Bind(R.id.plateNumber)
    TextView mPlateNumber;

    @Bind(R.id.number_of_park_unit)
    TextView mNumberOfParkUnit;

    @Bind(R.id.check_button)
    ImageButton checkButton;

    public ParkingUnitView(Context context) {
        super(context);
    }

    public ParkingUnitView(Context context, Berth berth) {
        super(context);
        mContext = context;
        mBerth = berth;
        initView();
    }

    public ParkingUnitView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public ParkingUnitView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public ParkingUnitView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item_parking_car, null);
        addView(view);

        ButterKnife.bind(this);

        if (mBerth != null) {
            setBerth(mBerth);
        }
    }

    public void setBerth(Berth mBerth) {
        refreshAllView();
        mPlateNumber.setText(mBerth.getPlateNumber());
        mNumberOfParkUnit.setText("泊位:" + mBerth.getBerthNum());
    }

    public void setCheckButtonBackground(){
        // Pop up dialog in Activity
        checkButton.setBackground(getResources().getDrawable(R.drawable.icon_exit_right));
    }

    private void refreshAllView(){
        checkButton.setBackground(null);
    }

}
