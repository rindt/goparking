package com.lesmtech.goparking;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.lesmtech.goparking.entity.Berth;
import com.lesmtech.goparking.entity.Employee;
import com.lesmtech.goparking.tool.GRetrofit;
import com.lesmtech.goparking.view.ConfirmLeftDialogView;
import com.lesmtech.goparking.view.ParkingUnitView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Response;

/**
 * @author Rindt
 * @version 0.1
 * @since 9/30/15
 */
public class CarExitActivity extends AppCompatActivity implements ConfirmLeftDialogView.DialogViewResultListener {

    private static final String TAG = "CarExitActivity";

    private int parkId;

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.search_container)
    EditText searchView;

    @Bind(R.id.search_result_panel)
    ListView resultListView;

    @Bind(R.id.no_result_layout)
    LinearLayout noResultLayout;

    // Start at 1
    private int page = 1;

    private boolean hasMoreData = true;

    private ArrayList<Berth> mBerths;

    private ArrayList<Berth> mBerthsAll;

    private BaseAdapter mCarUnitAdapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return mBerths.size();
        }

        @Override
        public Object getItem(int position) {
            return mBerths.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = new ParkingUnitView(CarExitActivity.this, mBerths.get(position));
            } else {
                ((ParkingUnitView) convertView).setBerth(mBerths.get(position));
            }
            // There is a bug, coz the view all has been created, so notify data will not trigger this method.
            // So the load-new-data will not be triggered
            // However, if the data more than 10, which can't be filled in a screen.
            // This method will be trigger when users scrolled down.
            if (hasMoreData && mBerths.size() < position + 5) {
                page++;
                new GetListOfCar().execute(page);
            }
            return convertView;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carexit);
        ButterKnife.bind(this);
        mBerths = new ArrayList<>();
        mBerthsAll = new ArrayList<>();

        SharedPreferences session = getSharedPreferences("session", MODE_PRIVATE);
        parkId = session.getInt(Employee.PARKID, -1);

        setSupportActionBar(mToolbar);
        mToolbar.setTitle(getResources().getString(R.string.title_car_exit));
        mToolbar.setNavigationIcon(R.drawable.arrow_back);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));

        resultListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((ParkingUnitView) view).setCheckButtonBackground();
                showConfirmDialog(mBerths.get(position));
            }
        });

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d("app_search", s.toString());

                mBerths = new ArrayList<>(mBerthsAll);

                ArrayList<Berth> needToBeRemovedIndex = new ArrayList<>();

                for (int i = 0; i < mBerths.size(); i++) {
                    Log.d("app", mBerths.get(i).getPlateNumber());
                    Log.d("app", s.toString().toLowerCase());
                    if (!mBerths.get(i).getPlateNumber().toLowerCase().contains(s.toString().toLowerCase())) {
                        needToBeRemovedIndex.add(mBerths.get(i));
                    }
                }
                for (int i = 0; i < needToBeRemovedIndex.size(); i++) {
                    mBerths.removeAll(needToBeRemovedIndex);
                }
                mCarUnitAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

        });
        // Request server to get result
        new GetListOfCar().execute(page);
    }

    public class GetListOfCar extends AsyncTask<Integer, Void, List<Berth>> {
        @Override
        protected List<Berth> doInBackground(Integer... params) {

            int page = params[0];

            try {
                Response<List<Berth>> response = GRetrofit.getInstance().showCarParkingListByState(String.valueOf(parkId), "1", page).execute();
                List<Berth> berths = response.body();
                if (berths != null && berths.size() != 0) {
                    return berths;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                hasMoreData = false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Berth> berths) {
            if (berths != null) {
                // Always keep the origin park units for searching
                if (berths.get(0).getId() != 0) {
                    mBerths.addAll(berths);
                    mBerthsAll = new ArrayList<>(mBerths);
                    if (resultListView.getAdapter() == null) {
                        resultListView.setAdapter(mCarUnitAdapter);
                    } else {
                        mCarUnitAdapter.notifyDataSetChanged();
                    }
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showConfirmDialog(Berth mBerth) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        ConfirmLeftDialogView view = new ConfirmLeftDialogView(CarExitActivity.this, mBerth);
        view.setmDialogViewResultListener(this);
        final AlertDialog mAlertDialog;
        mAlertDialog = mBuilder.setView(view).create();
        view.setDialog(mAlertDialog);
        WindowManager.LayoutParams wmlp = mAlertDialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.TOP;
        mAlertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
///                String result=data.getStringExtra("result");
                Log.d("dialog", "success");
                // Fresh the List or we can manually delete the one has been down.
                mBerths.clear();
                page = 1;
                new GetListOfCar().execute(page);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Log.d("dialog", "Cancel");
                mCarUnitAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void success(int code) {
        Intent intent = new Intent(this, BillDialogActivity.class);
        startActivityForResult(intent, code);
    }

    @Override
    public void fail() {
        Log.d(TAG, "Fail");
        mCarUnitAdapter.notifyDataSetChanged();
    }

}
