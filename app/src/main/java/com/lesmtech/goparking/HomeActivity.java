package com.lesmtech.goparking;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.lesmtech.goparking.asyntask.CashPayTask;
import com.lesmtech.goparking.entity.Employee;
import com.lesmtech.goparking.entity.ParkInfo;
import com.lesmtech.goparking.entity.json.SuccessResponse;
import com.lesmtech.goparking.fragment.home.CashRecordFragment;
import com.lesmtech.goparking.fragment.home.HelpFragment;
import com.lesmtech.goparking.fragment.home.PanelFragment;
import com.lesmtech.goparking.fragment.home.StatHistFragment;
import com.lesmtech.goparking.fragment.home.StatNowFragment;
import com.lesmtech.goparking.tool.DataFormatConvert;
import com.lesmtech.goparking.tool.GRetrofit;

import java.io.IOException;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static String TAG = "app_activity_home";

    public static String FRAGMENT = "fragment";

    private String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        getSupportFragmentManager().beginTransaction().replace(R.id.container, new PanelFragment()).commit();

        // UpdateLogInTime
        SharedPreferences session = getSharedPreferences("session", MODE_PRIVATE);
        uid = String.valueOf(session.getLong(Employee.ID, 0));
        new UpdateLastLogInTime().execute(uid);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemTextColor(getResources().getColorStateList(R.color.bg_nav_item));

        // doesn't work
        navigationView.setItemBackgroundResource(R.drawable.bg_nav_item);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_logout) {
//            deleteSession();
//            Intent intent = new Intent(this, SplashActivity.class);
//            startActivity(intent);
//            finish();
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_panel) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new PanelFragment(), null).commit();
        } else if (id == R.id.nav_changepsw) {
            // Handle the camera action
            Intent intent = new Intent(HomeActivity.this, ChangePasswordActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_help) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new HelpFragment(), null).commit();
        } else if (id == R.id.nav_history) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new StatHistFragment(), null).commit();
        } else if (id == R.id.nav_logout) {
            deleteSession();
            // Go back to SplashActivity
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_static) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new StatNowFragment(), null).commit();
        } else if (id == R.id.nav_cash) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new CashRecordFragment(), null).commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void displayToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void deleteSession() {
        SharedPreferences session = getSharedPreferences("session", MODE_PRIVATE);
        session.edit().clear().apply();
    }

    public class UpdateLastLogInTime extends AsyncTask<String, Void, Employee> {
        @Override
        protected Employee doInBackground(String... params) {
            String uid = params[0];
            try {
                Employee employee = GRetrofit.getInstance().updateEmployeeInfoTime(uid).execute().body();
                if (employee != null && employee.getId() != 0) {
                    return employee;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Employee employee) {
            if (employee != null) {
                SharedPreferences session = HomeActivity.this.getSharedPreferences("session", MODE_PRIVATE);
                SharedPreferences.Editor editor = session.edit();
                String landingTime = employee.getLandingTime();
                String convertTime = DataFormatConvert.uglyDataToDuration(landingTime);
                editor.putString(Employee.LANDINGTIME, convertTime);
                editor.apply();
            }
        }
    }
}
