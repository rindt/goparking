package com.lesmtech.goparking;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lesmtech.goparking.asyntask.CarEnterTask;
import com.lesmtech.goparking.entity.Berth;
import com.lesmtech.goparking.entity.Employee;
import com.lesmtech.goparking.tool.GRetrofit;
import com.lesmtech.goparking.view.ConfirmLeftDialogView;
import com.lesmtech.goparking.view.SpotReservedCarView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author Rindt
 * @version 0.1
 * @since 10/25/15
 */
public class SpotReservedActivity extends AppCompatActivity implements ConfirmLeftDialogView.DialogViewResultListener {

    private final String TAG = "SpotReservedActivity";

    private int parkId;

    private String employeeId;

    @Bind(R.id.lv_result)
    ListView showCarResultListView;

    @Bind(R.id.number_of_spot_occupied)
    TextView numberOfSpotOccupied;

    @Bind(R.id.search_container)
    EditText searchView;

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    private boolean hasMoreData = true;

    private int page = 1;

    private ArrayList<Berth> mBerths;

    private ArrayList<Berth> mBerthsAll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spotreserved);
        ButterKnife.bind(this);
        mBerths = new ArrayList<>();
        mBerthsAll = new ArrayList<>();

        // set toolbar
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.arrow_back);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.white));

        SharedPreferences session = getSharedPreferences("session", MODE_PRIVATE);
        parkId = session.getInt(Employee.PARKID, -1);
        employeeId = String.valueOf(session.getLong(Employee.ID, -1));

        // Request server to get cars
        if (parkId != -1) {
            // State 0 means the berth is occupied
            new GetCarResultFromServerByState().execute(String.valueOf(parkId), "2", String.valueOf(page));
        }

        // Search
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d("app_search", s.toString());

                mBerths = new ArrayList<>(mBerthsAll);

                ArrayList<Berth> needToBeRemovedIndex = new ArrayList<>();

                for (int i = 0; i < mBerths.size(); i++) {
                    Log.d("app", mBerths.get(i).getPlateNumber());
                    Log.d("app", s.toString().toLowerCase());
                    if (!mBerths.get(i).getPlateNumber().toLowerCase().contains(s.toString().toLowerCase())) {
                        needToBeRemovedIndex.add(mBerths.get(i));
                    }
                }
                for (int i = 0; i < needToBeRemovedIndex.size(); i++) {
                    mBerths.removeAll(needToBeRemovedIndex);
                }
                carResultListViewAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

        });

        // Click Listener to show dialog
        showCarResultListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //  Request to register the position
                Log.d(TAG, "Item Clicked.");

                Berth selectedBerth = mBerths.get(position);

                // Dialog or AddCarTask
                CarEnterTask task = new CarEnterTask(String.valueOf(parkId), selectedBerth.getPlateNumber(), employeeId);
                task.setmCarEnterTaskListerner(new CarEnterTask.CarEnterTaskListerner() {
                    @Override
                    public void successEnterCar(Berth berth) {
                        Log.d(TAG, "Success Enter Car!");
                        Toast.makeText(SpotReservedActivity.this, "成功录用车辆 " + berth.getPlateNumber(), Toast.LENGTH_SHORT);
                        // Update List
                        mBerths.clear();
                        page = 1;
                        new GetCarResultFromServerByState().execute(String.valueOf(parkId), "2", String.valueOf(page));
                    }

                    @Override
                    public void failureEnterCar() {
                        Log.d(TAG, "Failure Enter Car!");
                    }
                });
                task.execute();
            }
        });
    }

    private BaseAdapter carResultListViewAdapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return mBerths.size();
        }

        @Override
        public Object getItem(int position) {
            return mBerths.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = new SpotReservedCarView(SpotReservedActivity.this, mBerths.get(position));
            } else {
                ((SpotReservedCarView) convertView).setBerth(mBerths.get(position));
            }

            if (hasMoreData && mBerths.size() - 5 < position) {
                page++;
                new GetCarResultFromServerByState().execute(String.valueOf(parkId), "2", String.valueOf(page));
            }

            return convertView;
        }
    };

    @Override
    public void success(int code) {
        Intent intent = new Intent(this, BillDialogActivity.class);
        startActivityForResult(intent, code);
    }

    @Override
    public void fail() {
        Log.d(TAG, "fail");
        carResultListViewAdapter.notifyDataSetChanged();
    }

    public class GetCarResultFromServerByState extends AsyncTask<String, Void, List<Berth>> {
        @Override
        protected List<Berth> doInBackground(String... params) {

            String parkid = params[0];
            String state = params[1];
            int page = Integer.valueOf(params[2]);

            try {
                List<Berth> berths = GRetrofit.getInstance().getCarParkingListP(parkid, state, page).execute().body();
                if (berths != null && berths.size() != 0) {
                    return berths;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                Log.d("app", "No more data");
                hasMoreData = false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Berth> berths) {
            if (berths != null) {

                mBerths.addAll(berths);
                mBerthsAll = new ArrayList<>(mBerths);

                numberOfSpotOccupied.setText(String.valueOf(mBerths.size()));

                if (showCarResultListView.getAdapter() == null) {
                    showCarResultListView.setAdapter(carResultListViewAdapter);
                } else {
                    carResultListViewAdapter.notifyDataSetChanged();
                }
            } else {
                // Null handling
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
//                String result=data.getStringExtra("result");
                Log.d(TAG, "success");
                mBerths.clear();
                page = 1;
                new GetCarResultFromServerByState().execute(String.valueOf(parkId), "2", String.valueOf(page));
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Log.d(TAG, "Cancel");
                carResultListViewAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
