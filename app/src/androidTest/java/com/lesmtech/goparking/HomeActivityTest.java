package com.lesmtech.goparking;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

// View Matchers
// View Action
// Hamcrest

/**
 * @author Rindt
 * @version 0.1
 * @since 12/14/15
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class HomeActivityTest {

    @Rule
    public ActivityTestRule<SplashActivity> mSplashActivityRule = new ActivityTestRule<>(SplashActivity.class);

    @After
    public void clearUpAfter(){
        clearSession();
    }

    @Test
    public void signInFragmentShow() {
        onView(withId(R.id.title)).check(matches(isDisplayed()));
    }

    @Test
    public void signIn(){
        onView(withId(R.id.username)).perform(clearText(), typeText("2015"));
        onView(withId(R.id.id)).perform(clearText(),typeText("110"));
        onView(withId(R.id.password)).perform(clearText(),typeText("123"),closeSoftKeyboard());
        onView(withId(R.id.action_submit)).perform(click());
    }

    private void clearSession() {
        SharedPreferences session = mSplashActivityRule.getActivity().getSharedPreferences("session", Context.MODE_PRIVATE);
        session.edit().clear().commit();
    }

}
