package com.lesmtech.goparking;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.lesmtech.goparking.entity.Berth;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

// View Matchers
// View Action
// Hamcrest

/**
 * @author Rindt
 * @version 0.1
 * @since 12/14/15
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class SpotReservedActivityTest {

    private ArrayList<Berth> mockupBerth = new ArrayList<>();

    private int mockupSize = 10;

    @Before
    public void setUp() {
        for (int i = 0; i < mockupSize; i++) {
            Berth berth = new Berth(i, i, i, i, String.valueOf(i), String.valueOf(i), String.valueOf(i), i, String.valueOf(i), i, i, i, i, String.valueOf(i));
            mockupBerth.add(berth);
        }
    }

    @Rule
    public ActivityTestRule<SpotReservedActivity> mSpotReservedActivityActivityTestRule = new ActivityTestRule<>(SpotReservedActivity.class);

    @Test
    public void checkAdapter() {
//        onData(withText("1")).check(matches(isDisplayed()));
    }


}
