package com.lesmtech.goparking;

import android.content.res.AssetFileDescriptor;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.lesmtech.goparking.tool.Utils;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * This class is to test Utils
 * @author Rindt
 * @version 0.1
 * @since 12/15/15
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class UtilsTest {

    String[] numbersInput = new String[]{
            "000", "01", "12", "440", "567", "999"
    };

    String[] numbersOutPut = new String[]{
            "000", "001", "012", "440", "567", "999"
    };

    @Test
    public void testNumberOfDigits() {
        for (int i = 0; i < numbersInput.length; i++) {
            Assert.assertEquals(Utils.formatBerthNumberDigits(numbersInput[i]), numbersOutPut[i]);
        }
    }

}
