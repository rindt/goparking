package com.lesmtech.goparking;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.LargeTest;
import android.test.suitebuilder.annotation.MediumTest;
import android.test.suitebuilder.annotation.SmallTest;

import com.lesmtech.goparking.entity.Berth;
import com.lesmtech.goparking.entity.Employee;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;

// View Matchers
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;

// View Action
import static android.support.test.espresso.action.ViewActions.click;

// Hamcrest
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

/**
 * @author Rindt
 * @version 0.1
 * @since 12/14/15
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class SplashActivityTest {

    @Rule
    public ActivityTestRule<SplashActivity> mSplashActivityRule = new ActivityTestRule<>(SplashActivity.class);

    @After
    public void clearUpAfter(){
        clearSession();
    }

    @Test
    public void signInFragmentShow() {
        onView(withId(R.id.title)).check(matches(isDisplayed()));
    }

    @Test
    public void signIn(){
        onView(withId(R.id.username)).perform(clearText(), typeText("2015"));
        onView(withId(R.id.id)).perform(clearText(),typeText("110"));
        onView(withId(R.id.password)).perform(clearText(),typeText("123"),closeSoftKeyboard());
        onView(withId(R.id.action_submit)).perform(click());
    }

    private void clearSession() {
        SharedPreferences session = mSplashActivityRule.getActivity().getSharedPreferences("session", Context.MODE_PRIVATE);
        session.edit().clear().commit();
    }

}
